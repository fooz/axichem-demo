(function() {
    var dataNew = [];

    function ajax_get_tables() {

        jQuery.ajax({
            type: "GET",
            url: tinyMCE_object.path_local + '/xhr/list_shortcode.php',
            timeout: 6000,
            data: {
                id: jQuery('input#post_ID').val()
            },
            success: function(result) {
                //console.log(result);
                dataNew = JSON.parse(result);

                // Add button >>>>

                tinymce.PluginManager.add('mybutton', function(editor, url) {
                    var self = this,
                        button;

                    editor.addButton('mybutton', {
                        text: tinyMCE_object.button_name,
                        icon: false,
                        onclick: function() {
                            editor.windowManager.open({
                                title: tinyMCE_object.button_title,
                                body: [{
                                        type: 'listbox',
                                        id: 'shortcode',
                                        name: 'shortcode',
                                        label: 'Your shortcode',
                                        values: dataNew

                                    }
                                ],
                                onsubmit: function(e) {
                                    editor.insertContent('['+ e.data.shortcode +']');
                                }
                            });
                        },
                    });
                });

                // <<<<< Add button

            },
            error: function(result) {}
        });
    }
    ajax_get_tables();
})();
