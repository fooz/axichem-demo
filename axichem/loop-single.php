<div class="loop"> <!-- loop-single -->
<?php

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>
			<div class="row">
				<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>

				<?php
					if ( has_post_thumbnail() ) {
				?>
						<div class="full_width_element">
						<?php

							$atr = array(
								'class' => "img-responsive text-center"
							);
							the_post_thumbnail('slider', $atr);
						?>
						</div>
				<?php
					}
				?>
					<div class="col-xs-12">
                        <?php
                            the_content();
                        ?>
					</div>

				</article>
			</div>
<?php	} // end while
	}
	else {
	?>
			<div class="row">
				<div class="col-xs-12">
					<h2>
				<?php echo _e( 'Nothing to Show Right Now', 'theme'); ?>
					</h2>
				</div>
			</div>
	<?php
	} // end if
wp_reset_query();
//wp_reset_postdata()
?>
</div> <!-- /.row -->
