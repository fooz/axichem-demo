<!-- content boxed -->
<section id="content">
	<?php
	//get_post_type( $post );
	if(is_404()){
		get_template_part('loop', '404');
	}else if (is_page()) {
		get_template_part('loop', 'single');
	}else if (is_singular()) {
		get_template_part('loop', 'post');
	} else {
		get_template_part('loop');
	}
	?>
</section>
