<?php
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    require('../../../../wp-load.php');
else
    require('wp-load.php');
?>

<div class="item show">
<?php
// header
if(strlen($_GET['name']) > 0)
    $header = $_GET['name'];

// Category
if(strlen($_GET['category']) > 0)
    $category = $_GET['category'];

$category = explode(',', $category);

$tax_query = array();
$tax_query[] = array(
    'taxonomy' => 'type',
    'field' => 'slug',
    'terms' => $category
);

// Year
if(strlen($_GET['year']) > 0)
    $year = $_GET['year'];

$date_query = array();
$date_query = array(
    'year' => $year
);

$queryFeed = new WP_Query(
    array(
        'post_type' => 'news',
        'posts_per_page' => -1,
        'date_query' => $date_query,
        'tax_query' => $tax_query,
        'order' => "DESC",
        'orderby' => 'date',
        'ignore_sticky_posts' => 1,
        'post_status' => 'publish'
    )
);
?>

<?php if($queryFeed->post_count == 0){ ?>
    No results
<?php } ?>

<!-- <div class="shortcode_header text38"><?php echo $header; ?></div> -->

<?php
$x = 1;
while ($queryFeed->have_posts()) : $queryFeed->the_post();
?>

<div class="post">
    <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
    <a class="title text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
    <div class="excerpt text15">
        <?php echo limit_words(get_the_excerpt(), 10); ?>
        <a class="readmore" href="<?php echo get_permalink(); ?>">
            <?php
            $read_more = 'Read the article here';
            if(strlen(get_field('read_more', get_the_ID())) > 0)
                $read_more = get_field('read_more', get_the_ID());
            echo __($read_more, 'axichem');
            ?>
        </a>
    </div>

    <a class="arrow" href="<?php echo get_permalink(); ?>">
        <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
            <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
            <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
            <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
        </svg>
    </a>
</div>

<?php
if($x%3 == 0)
    echo '</div><div class="item">';
?>

<?php
    $x++;
endwhile;
wp_reset_query();
?>
</div>
