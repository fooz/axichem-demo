<?php
function shortcode_PagesDisplay( $atts ) {
    ob_start();
    $act = get_the_ID();
?>

<div class="shortcode shortcode_pagesdisplay">
    <div class="containerXX">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="links">
                    <?php
                    $menuOrg = wp_get_nav_menu_items('Menu CorporateGovernance');
                    foreach ($menuOrg as $item) {
                        $url = get_permalink($item->object_id);
                        if ($item->type == 'custom') {
                            $url = $item->url;
                        }
                        $getpost = get_post($item->object_id);

                        $active = '';
                        if(get_the_ID() == $item->object_id)
                            $active = 'active';
                        echo '<a href="'. $url .'" class="'. $active .' '. $item->classes[0] .'" data-id="'. $item->object_id .'">'. $getpost->post_title. '</a>';
                    }
                    wp_reset_query();
                    ?>
                </div>
            </div>
            <div class="col-12 col-lg-7 offset-lg-1">
                <div class="title_page text38"><?php echo get_the_title(); ?></div>
                <div class="content text15">
                    <?php
                    $content = get_the_content();
                    $content = preg_replace('/<h1>/i', '<h1 class="text38">', $content);
                    $content = preg_replace('/<h2>/i', '<h2 class="text38">', $content);
                    $content = preg_replace('/<h3>/i', '<h3 class="text38">', $content);
                    $content = preg_replace('/<h4>/i', '<h4 class="text32">', $content);
                    $content = preg_replace('/<h5>/i', '<h5 class="text32">', $content);
                    $content = preg_replace('/<h6>/i', '<h6 class="text32">', $content);
                    echo do_shortcode($content);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $display = ob_get_contents();
    ob_end_clean();
    return $display;
}
add_shortcode('pages_display', 'shortcode_PagesDisplay' );
