<?php
function shortcode_PressRoom( $atts ) {
    ob_start();
    $act = get_the_ID();
?>

<div class="shortcode shortcode_pressroom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="links">
                    linkiii oress
                </div>
            </div>
            <div class="col-12 col-lg-7 offset-lg-1">
                <div class="title_page text38"><?php echo get_the_title(); ?></div>
                <div class="content text15">
                    <?php
                    $content = get_the_content();
                    $content = preg_replace('/<h1>/i', '<h1 class="text38">', $content);
                    $content = preg_replace('/<h2>/i', '<h2 class="text38">', $content);
                    $content = preg_replace('/<h3>/i', '<h3 class="text38">', $content);
                    $content = preg_replace('/<h4>/i', '<h4 class="text32">', $content);
                    $content = preg_replace('/<h5>/i', '<h5 class="text32">', $content);
                    $content = preg_replace('/<h6>/i', '<h6 class="text32">', $content);
                    echo do_shortcode($content);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $display = ob_get_contents();
    ob_end_clean();
    return $display;
}
add_shortcode('pressroom_display', 'shortcode_PressRoom' );
