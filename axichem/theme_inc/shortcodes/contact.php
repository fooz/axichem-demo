<?php
function shortcode_contact( $atts ) {
    ob_start();
?>

    <div class="shortcode shortcode_contact">
        <?php if(is_array($atts) && isset($atts['name'])){ ?>
            <div class="name text22"><?php echo $atts['name'] ?></div>
        <?php } ?>
        <?php if(is_array($atts) && isset($atts['email'])){ ?>
            <a class="email text15" href="mailto:<?php echo $atts['email'] ?>">
                <span class="icon">
                    <svg x="0px" y="0px" width="18.8px" height="18.8px" viewBox="0 0 18.8 18.8" style="enable-background:new 0 0 18.8 18.8;" xml:space="preserve">
                    	<path class="st0" d="M18.3,4.6C18,3.4,17,2.3,15.6,2.3h-0.1l0,0H3.2c-1.6,0-2.9,1.2-2.9,2.9v8.4l0,0c0,1.6,1.2,2.9,2.9,2.9h12.3
                    		c1.4,0,2.6-1,2.8-2.3c0-0.2,0.1-0.4,0.1-0.6V5.2C18.4,5,18.3,4.8,18.3,4.6z M17.5,5.2v8.4c0,1.1-0.9,1.9-1.9,1.9H3.2
                    		c-1.1,0-1.9-0.9-1.9-1.9V5.2c0-0.1,0-0.2,0-0.2l7.9,5.1l8.1-5.2C17.5,5,17.5,5.1,17.5,5.2z M15.6,3.3c0.6,0,1.1,0.3,1.5,0.8
                    		L9.3,8.9L1.7,4c0.4-0.5,0.9-0.8,1.5-0.8H15.6z"/>
                    </svg>
                </span>
                <?php echo $atts['email'] ?>
            </a>
        <?php } ?>
        <?php if(is_array($atts) && isset($atts['phone'])){ ?>
            <a class="phone text15" href="tel:<?php echo $atts['phone'] ?>">
                <span class="icon">
                    <svg x="0px" y="0px" width="18.8px" height="18.8px" viewBox="0 0 18.8 18.8" style="enable-background:new 0 0 18.8 18.8;" xml:space="preserve">
                    <path fill="none" stroke-width="2" d="M17.1,0.5h-3.4c-0.5,0-1,0.6-1,1.1c0,1.2-0.2,2.4-0.5,3.5c-0.1,0.3,0,0.8,0.2,1l2.1,2.1c-1.4,2.7-3.6,5-6.4,6.4
                    	l-2.1-2.1c-0.3-0.3-0.6-0.3-1-0.2C4,12.6,3,12.8,1.8,12.8c-0.5,0-0.8,0.4-0.8,1v3.4c0,0.5,0.2,1,0.8,1c9.1,0,16.2-7.3,16.2-16.4
                    	c0,0,0,0,0,0C18,1.1,17.7,0.5,17.1,0.5z"/>
                    </svg>
                </span>
                <?php echo $atts['phone'] ?>
            </a>
        <?php } ?>
    </div>

<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('contact', 'shortcode_contact' );
