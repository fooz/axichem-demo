<?php
function shortcode_People( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_people">
    <div class="people">
        <?php
        $terms = get_terms('position', array(
            'hide_empty' => false,
        ));
        $categories = $atts['categories'];
        foreach ($terms as $keyTerm => $valueTerm) {
            if($valueTerm->slug == $categories){
                $category_id = $valueTerm->term_id;
                $category_name = $valueTerm->name;
            }
        }
        ?>

        <div class="row">
            <div class="col-12 col-md-4">
                <div class="category text15"><?php echo $category_name; ?></div>
            </div>
        </div>
        <div class="list">
            <?php
            $tax_query = array();
            $tax_query[] = array(
                'taxonomy' => 'position',
                'field' => 'slug',
                'terms' => $categories
            );
            $queryPeople = new WP_Query(
                array(
                    'post_type' => 'people',
                    'posts_per_page' => -1,
                    'tax_query' => $tax_query,
                    'order' => "DESC",
                    'orderby' => 'date',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            $x = 1;
            while ($queryPeople->have_posts()) : $queryPeople->the_post();
            ?>

            <div class="person">
                <div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large', true)[0]; ?>)"></div>
                <div class="desc">
                    <div class="title text22" href=""><?php echo get_the_title(); ?></div>
                    <div class="description text15" href=""><?php echo get_the_content(); ?></div>
                </div>
            </div>

            <?php
                $x++;
            endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>

<?php
    $display = ob_get_contents();
    ob_end_clean();
    return $display;
}
add_shortcode('people', 'shortcode_People' );
