<?php
function shortcode_Events( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_scroll shortcode_events">
    <div class="shortcode_header text38"><?php _e('Latest Events', 'axichem')?></div>
    <div class="events items_container" data-p="0">
        <div class="items">
            <div class="item">
            <?php
            $queryOfferAll = new WP_Query(
                array(
                    'post_type' => 'events',
                    'posts_per_page' => -1,
                    'order' => "DESC",
                    'orderby' => 'date',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            $x = 1;
            while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
            ?>

            <div class="event">
                <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                <div class="title text22" href=""><?php echo get_the_title(); ?></div>
                <a class="add-calendar text13" target="_blank" href="https://calendar.google.com/calendar/r/eventedit?text=<?php echo get_the_title(); ?>&dates=<?php echo __(get_the_date('Ymd')); ?>T010000Z/<?php echo __(get_the_date('Ymd')); ?>T020000Z&details=Event+Details+Here&sf=true">
                    <svg x="0px" y="0px" width="14px" height="14.1px" viewBox="0 9.525 14 14.1" enable-background="new 0 9.525 14 14.1" xml:space="preserve">
                    <path d="M10.3,10.625v-1.1h-1v1.1h-5v-1.1h-1v1.1H0v13h14v-13H10.3z M3.3,11.625v1h1v-1h5v1h1v-1H13v2.5H1v-2.5H3.3z
                    	 M1,22.725v-7.6h12v7.6H1z"/>
                    </svg>
                    <span><?php echo __('Add to calendar', 'axichem'); ?></span>
                </a>
            </div>

            <?php
            if($x%3 == 0)
                echo '</div><div class="item">';
            ?>

            <?php
                $x++;
            endwhile;
            wp_reset_query();
            ?>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="arrows">
            <a class="next">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="11.604,0.354 6.104,6.104 0.354,0.354 "/>
                </svg>
            </a>
            <a class="prev off">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="0.361,6.465 5.861,0.715 11.611,6.465 "/>
                </svg>
            </a>
        </div>
    </div>
</div>



<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('events', 'shortcode_Events' );
