<?php
function shortcode_CisionFeedSelected( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_scroll shortcode_cision_feed1">
    <div class="shortcode_header text38"><?php echo __('News', 'axichem'); ?></div>
    <div class="posts items_container" data-p="0">
        <div class="items">
            <div class="item show">
            <?php
            $queryOfferAll = new WP_Query(
                array(
                    'post_type' => 'news',
                    'posts_per_page' => 20,
                    'order' => "DESC",
                    'post__in' => explode(',',$atts['items']),
                    'orderby' => 'date',
                    'ignore_sticky_posts' => 1,
                    'post_status' => 'publish'
                )
            );
            $x = 1;
            while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
            ?>

            <div class="post">
                <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                <a class="title text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
                <div class="excerpt text15">
                    <?php echo limit_words(get_the_excerpt(), 10); ?>
                    <a class="readmore" href="<?php echo get_permalink(); ?>"><?php echo __('Read the article here', 'axichem'); ?></a>
                </div>

                <a class="arrow" href="<?php echo get_permalink(); ?>">
                    <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
                        <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
                        <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
                        <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
                    </svg>
                </a>
            </div>

            <?php
            if($x%3 == 0)
                echo '</div><div class="item">';
            ?>

            <?php
                $x++;
            endwhile;
            wp_reset_query();
            ?>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="arrows">
            <a class="next">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="11.604,0.354 6.104,6.104 0.354,0.354 "/>
                </svg>
            </a>
            <a class="prev off">
                <svg x="0px" y="0px" width="11.965px" height="6.818px" viewBox="0 0 11.965 6.818" enable-background="new 0 0 11.965 6.818" xml:space="preserve">
                <polyline fill="none" stroke-width="2" stroke-miterlimit="10" points="0.361,6.465 5.861,0.715 11.611,6.465 "/>
                </svg>
            </a>
        </div>
    </div>
</div>



<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('feed_selected', 'shortcode_CisionFeedSelected' );
