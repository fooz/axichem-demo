<?php
function shortcode_Files( $atts ) {
    ob_start();
?>

<div class="shortcode shortcode_files">
    <?php
    $items = explode(',', $atts['items']);
    foreach ($items as $key => $value) {
         $item = get_post($value);

         $target = '';
         $href = get_permalink($value);
         if($item->post_type == 'attachment'){
             $target = '_blank';
             $href = wp_get_attachment_url($value);
         }
         ?>

         <div class="item">
             <div class="col-first">
                 <div class="date text15"><?php echo __(date('F d, Y', strtotime($item->post_date))); ?></div>
                 <a class="title text22" href="<?php echo $href; ?>" target="<?php echo $target; ?>"><?php echo get_the_title($value); ?></a>
            </div>
            <div class="col-second">
                <?php if(get_post_meta($value, 'FilesUrl', true) ){ ?>
                    <a class="download" href="<?php echo get_post_meta($value, 'FilesUrl', true ); ?>" target="_blank">
                        <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                            <line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"></line>
                            <polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18"></polyline>
                            <polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3"></polyline>
                        </svg>
                    </a>
                <?php } ?>

                <?php if($item->post_type == 'attachment'){ ?>
                    <a class="download" href="<?php echo $href; ?>" target="_blank">
                        <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                            <line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"></line>
                            <polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18"></polyline>
                            <polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3"></polyline>
                        </svg>
                    </a>
                <?php }else{ ?>
                    <a class="arrow" href="<?php echo $href; ?>">
                        <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
                            <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
                            <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
                            <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
                        </svg>
                    </a>
                <?php } ?>

            </div>
        </div>

         <?php
    }
    ?>
</div>

<?php
$display = ob_get_contents();
ob_end_clean();
return $display;
}
add_shortcode('files', 'shortcode_Files' );
