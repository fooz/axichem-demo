<?php
// Custom post type

require_once(THEME_CPT_DIRECTORY.'cision.php');
require_once(THEME_CPT_DIRECTORY.'reports.php');
require_once(THEME_CPT_DIRECTORY.'events.php');
require_once(THEME_CPT_DIRECTORY.'meetings.php');
require_once(THEME_CPT_DIRECTORY.'patents.php');
require_once(THEME_CPT_DIRECTORY.'people.php');
require_once(THEME_CPT_DIRECTORY.'financial-calendar.php');
