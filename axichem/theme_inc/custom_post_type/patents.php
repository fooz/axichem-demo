<?php
// Patents

function register_patents_init() {
    $args = array(
        'public' => true,
        'label'  => 'Patents',
        'rewrite' => array('slug' => 'patents'),
        'menu_icon'  => 'dashicons-welcome-learn-more',
        'supports' => array('title','editor')
    );
    register_post_type('patents', $args );
}
add_action( 'init', 'register_patents_init' );

register_taxonomy('patents-category', 'patents',
    array(
        'hierarchical' => true,
        'label' => __('Category'),
        'rewrite' => array('slug' => 'patents-category'),
        'hierarchical' => true,
        'rewrite' => true
    )
);
