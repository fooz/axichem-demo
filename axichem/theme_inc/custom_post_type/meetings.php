<?php
// Patents

function register_meetings_init() {
    $args = array(
        'public' => true,
        'label'  => 'Meetings',
        'rewrite' => array('slug' => 'meetings'),
        'menu_icon'  => 'dashicons-welcome-learn-more',
        'supports' => array('title')
    );
    register_post_type('meetings', $args );
}
add_action( 'init', 'register_meetings_init' );

register_taxonomy('meetings-category', 'meetings',
    array(
        'hierarchical' => true,
        'label' => __('Category'),
        'rewrite' => array('slug' => 'meetings-category'),
        'hierarchical' => true,
        'rewrite' => true
    )
);
