<?php

// Custom post type Przykład

add_action( 'init', 'przyklad_init' );
/*
 * Register a book post type.
		Menu position
		5 - below Posts
		10 - below Media
		15 - below Links
		20 - below Pages
		25 - below comments
		60 - below first separator
		65 - below Plugins
		70 - below Users
		75 - below Tools
		80 - below Settings
		100 - below second separator
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function przyklad_init() {
	$labels = array(
		'name'               => _x( 'Przykład', 'post type general name', 'engine' ),
		'singular_name'      => _x( 'Przykład', 'post type singular name', 'engine' ),
		'menu_name'          => _x( 'Przykład', 'admin menu', 'engine' ),
		'name_admin_bar'     => _x( 'Przykład', 'add new on admin bar', 'engine' ),
		'add_new'            => _x( 'Dodaj nowy', 'zestaw mebli', 'engine' ),
		'add_new_item'       => __( 'Dodaj nowy', 'engine' ),
		'new_item'           => __( 'Nowe', 'engine' ),
		'edit_item'          => __( 'Edytuj', 'engine' ),
		'view_item'          => __( 'Pokaż', 'engine' ),
		'all_items'          => __( 'Wszystkie przykłady', 'engine' ),
		'search_items'       => __( 'Szukaj w przykładach', 'engine' ),
		'parent_item_colon'  => __( 'Rodzić przykładu:', 'engine' ),
		'not_found'          => __( 'Nic nie znaleziono.', 'engine' ),
		'not_found_in_trash' => __( 'nic nie znaleziono w koszu', 'engine' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'przyklad' ),
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       =>  true,
		'menu_position' 	 	 => 20,
		 'menu_icon' 		 		 => '',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', /*'excerpt', 'comments'*/ ),
		'taxonomies' 				 => array('przyklad_cat'),
	);
	register_post_type( 'przyklad', $args );
}


function add_przyklad_icons_styles(){
//dashicons
?>
<style>
#adminmenu .menu-icon-przyklad div.wp-menu-image:before {
  content: '\f313';
}
</style>

<?php
}
add_action( 'admin_head', 'add_przyklad_icons_styles' );


/*
 * 		ADD META BOXES
*/
add_action('add_meta_boxes', 'fpweb_przyklad_meta_box');
add_action('save_post', 'fpweb_przyklad_save_post');


function fpweb_przyklad_meta_box()
{
/*	$meta_box_title = __('Courses Setings','engine');

	add_meta_box('fpweb_courses_settings', $meta_box_title, 'fpweb_courses_html', 'courses', 'advanced');*/
	// if necesery you can add in outhers type
	/*
	add_meta_box('fpweb_courses_settings', $meta_box_title, 'fpweb_courses_html', 'page', 'normal');
	add_meta_box('fpweb_courses_settings', $meta_box_title, 'fpweb_courses_html', 'post', 'normal');
	add_meta_box('fpweb_courses_settings', $meta_box_title, 'fpweb_courses_html', 'portfolio', 'normal');
	add_meta_box('fpweb_courses_settings', $meta_box_title, 'fpweb_courses_html', 'gallery', 'normal'); */
}

/*
function fpweb_courses_html($post)
{
	$post_id = $post->ID;


	# Fields
	$custom_color_title = get_post_meta($post_id, 'custom_color_title', TRUE);

?>
<style>
	#fpweb_courses_settings .inside {
		margin: 0px;
		padding: 0px;
	}

	#fpweb_courses_settings > label {
		width: 20%;
		display: inline-block;
		float: left;
		font-weight: bold;
	}


	#fpweb_courses_settings .field{
		float: left;
		width:80%;
	}

	#fpweb_courses_settings .field label {
		margin-right: 10px;
	}

	#fpweb_courses_settings .desc {
		display: block;
		color: #999;
		font-style: italic;
		margin-top: 8px;
	}

	#fpweb_courses_settings .clear {
		clear: both;
	}

</style>

<?php
$colorset = array(
				array( 'primary',  _x('pink (default)','color courses list','engine')),
				array( 'success',  _x('green','color courses list','engine')),
				array( 'warning',  _x('yellow','color courses list','engine')),
				array( 'danger',  _x('red','color courses list','engine')),
				array( 'white',  _x('white','color courses list','engine')),
			);
?>

<div class="misc-pub-section fpweb_courses_settings clear" id="fpweb_courses_settings">

	<label for="custom_page_title">Color</label>

	<div class="field">
		<div class="field-container">
			<select name="custom_color_title" id="custom_color_title" class="regular-text">
				<?php

					foreach($colorset as $color){
					echo '<option value="'.$color[0].'" ';
					if(esc_attr($custom_color_title) == $color[0])
						echo 'selected="selected"';
					echo '>'.$color[1].'</option>';
				}?>
			</select>
		</div>
		<div class="desc"><? _e('Leave empty if you want to use white cours title.','fpweb_framework'); ?></div>
	</div>
	<div class="clear"></div>
</div>

<?php

}
*/

function fpweb_przyklad_save_post($post_id)
{

/*	if( defined("DOING_AUTOSAVE"))
		return;

	# Set Field Contents
	$custom_color_title 				= isset($_POST['custom_color_title']) ? $_POST['custom_color_title'] : '';

	update_post_meta($post_id, 'custom_color_title', trim($custom_color_title));*/
}


/* REGISTER TAXONOMY */


add_action( 'init', 'create_przyklad_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "courses"
function create_przyklad_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Kategoria przykładów', 'taxonomy/kategoria nazwa', 'engine' ),
		'singular_name'     => _x( 'Kategoria', 'taxonomykategoria nazwa w l.m', 'engine' ),
		'search_items'      => __( 'Szukaj', 'engine' ),
		'all_items'         => __( 'Wszystkie', 'engine' ),
		'parent_item'       => null,
		'parent_item_colon' => null,
		'edit_item'         => __( 'Edytuj', 'engine' ),
		'update_item'       => __( 'Aktualizuj', 'engine' ),
		'add_new_item'      => __( 'Dodaj nowy', 'engine' ),
		'new_item_name'     => __( 'Nowa nazwa', 'engine' ),
		'menu_name'         => __( 'Kategoria przykładów', 'engine' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'przyklad_cat' ),
	);
	/*register_taxonomy( $taxonomy, $object_type, $args );*/
	register_taxonomy( 'przyklad_cat', 'przykladd' , $args );


}

/*// Custom Taxonomies for qtranslate
add_action('courses_cat_add_form',		'qtrans_modifyTermFormFor');
add_action('courses_cat_edit_form',		'qtrans_modifyTermFormFor');*/


?>
