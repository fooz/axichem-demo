<?php
function register_events_init() {
    $args = array(
        'public' => true,
        'label'  => 'Events',
        'rewrite' => array('slug' => 'events'),
        'menu_icon'  => 'dashicons-megaphone',
        'supports' => array('title','editor','thumbnail')
    );
    register_post_type('events', $args );
}
add_action( 'init', 'register_events_init' );
