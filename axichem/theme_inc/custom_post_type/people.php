<?php
// People

function register_people_init() {
    $args = array(
        'public' => true,
        'label'  => 'People',
        'rewrite' => array('slug' => 'people'),
        'menu_icon'  => 'dashicons-admin-users',
        'supports' => array('title','editor','excerpt','thumbnail')
    );
    register_post_type('people', $args );
}
add_action( 'init', 'register_people_init' );

register_taxonomy('position', 'people',
    array(
        'hierarchical' => true,
        'label' => __('Position'),
        'rewrite' => array('slug' => 'position'),
        'hierarchical' => true,
        'rewrite' => true
    )
);
