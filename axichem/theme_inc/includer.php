<?php
/*

*/

define( 'THEME_PLUGIN_DIRECTORY', get_template_directory() . '/theme_inc/plugins/' );
define( 'THEME_PLUGIN_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/plugins/' );
define( 'THEME_CPT_DIRECTORY', get_template_directory() . '/theme_inc/custom_post_type/' );
define( 'THEME_CPT_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/custom_post_type/' );
define( 'THEME_SC_DIRECTORY', get_template_directory() . '/theme_inc/shortcodes/' );
define( 'THEME_SC_DIRECTORY_URL', get_template_directory_uri() . '/theme_inc/shortcodes/' );


/*inlude PLUGINS*/
require_once(THEME_PLUGIN_DIRECTORY.'includer.php');
require_once(THEME_CPT_DIRECTORY.'includer.php');
require_once(THEME_SC_DIRECTORY.'includer.php');
?>
