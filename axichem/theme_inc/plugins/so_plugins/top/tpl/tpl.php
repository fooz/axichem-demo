<?php
$widget_name = "top";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-top so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <?php if(strlen($instance['background']) > 0 && $instance['background'] != 0){ ?>
        <div class="container-background">
            <div class="background" style="background-image: url(<?php if(strlen($instance['background']) > 1) echo wp_get_attachment_image_src($instance['background'], 'large', true)[0]; ?>)"></div>
        </div>
    <?php } ?>

    <div class="content <?php if(strlen($instance['background']) == 0 || $instance['background'] == 0) echo 'background_none'; ?>">
        <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-5 col-lg-5">
                <div class="text1 text38"><?php echo $instance['text1']; ?></div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-5">
                <div class="text2 text22"><?php echo $instance['text2']; ?></div>
            </div>
            <div class="col-12 col-md-3 col-lg-2">
                <a class="text15 readmore">
                    Read more
                    <svg x="0px" y="0px" width="15.083px" height="18.583px" viewBox="0 0 15.083 18.583" enable-background="new 0 0 15.083 18.583" xml:space="preserve">
                    	<polyline fill="none" stroke-miterlimit="10" points="0.213,10.79 7.512,17.804 14.603,10.99 	"/>
                    	<line fill="none" stroke-miterlimit="10" x1="7.5" y1="18" x2="7.5" y2="0"/>
                    </svg>

                </a>
            </div>
        </div>
        </div>
    </div>
</div>
