var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    jQuery(document).on(clickHandler, '.widget-top .readmore', function(event) {
        nextWidget = jQuery('.widget-top').next('.widget');
        if(nextWidget.length != 1)
            nextWidget = jQuery('.widget-top').next().next('.widget');
        jQuery('html, body').animate({
            scrollTop: nextWidget.position().top - jQuery('header').height()
        }, 600);
    });
});
