var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    jQuery(window).load(function() {
        setTimeout(function() {
            if (jQuery('.widget-search .top form input[type="text"]').length == 1 && window.location.search.length == 3) {
                jQuery('.widget-search .top form input[type="text"]').trigger('focus');
            }
        }, 1500);
    })
});
