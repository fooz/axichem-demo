<?php
$widget_name = "search";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-search so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header text38">
                        <?php
                        if(strlen($_GET['s']) > 0){
                            echo __('Search Results', 'axichem');
                        }else{
                            echo __('Search', 'axichem');
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-8">
                    <form role="search" method="get" id="searchform" action="<?php bloginfo('home'); ?>">
                        <input class="text38" type="text" name="s" placeholder="<?php echo $_GET['s']; ?>" />
                        <button type="submit">
                            <svg width="17.443px" height="30px" viewBox="0 0 17.443 30" enable-background="new 0 0 17.443 30" xml:space="preserve">
                            <path d="M11.686,17l-0.277-0.444c2.333-2.718,2.021-6.901-0.698-9.234C7.992,4.988,3.897,5.257,1.564,7.975
                                c-2.333,2.719-2.021,6.791,0.698,9.124c2.43,2.086,6.029,2.075,8.458-0.011L11,17.355v0.787l4.971,4.971l1.477-1.302L12.473,17
                                H11.686z M6.507,17c-0.004,0-0.009,0-0.013,0c-2.476,0-4.483-2.183-4.483-4.66c0-2.476,2.007-4.572,4.483-4.572
                                c2.477,0,4.484,2.14,4.484,4.616C10.982,14.857,8.98,17,6.507,17z"/>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class="score">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-7">
                    <div class="header text38">
                        <?php echo $_GET['s']; ?>
                        <?php
                        if(strlen($_GET['s']) > 0){
                            echo __('search results', 'axichem');
                        }
                        ?>
                    </div>
                    <?php
                    if(strlen($_GET['s']) > 0){
                        $queryFeed = new WP_Query(
                            array(
                                'post_type' => 'page news',
                                'posts_per_page' => -1,
                                's' => $_GET['s'],
                                'order' => "DESC",
                                'orderby' => 'date',
                                'ignore_sticky_posts' => 1,
                                'post_status' => 'publish'
                            )
                        );
                        while ($queryFeed->have_posts()) : $queryFeed->the_post();
                            global $post;
                            $postId = $post->ID;
                    ?>

                    <div class="post">
                        <div class="col-first">
                            <div class="date text15"><?php echo __(get_the_date('F d, Y')); ?></div>
                            <a class="title text22" href="<?php echo get_permalink(); ?>"><?php echo get_the_title($postId); ?></a>
                            <div class="excerpt text15">
                                <?php echo limit_words(get_the_excerpt(), 10); ?>
                                <a class="readmore" href="<?php echo get_permalink($postId); ?>">
                                    <?php
                                    $read_more = 'Read the article here';
                                    if(strlen(get_field('read_more', $postId)) > 0)
                                        $read_more = get_field('read_more', $postId);
                                    echo __($read_more, 'axichem');
                                    ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-second">
                            <?php if(get_post_meta($postId, 'FilesUrl', true) ){ ?>
                                <a class="download" href="<?php echo get_post_meta($postId, 'FilesUrl', true ); ?>" target="_blank">
                                    <svg x="0px" y="0px" width="24px" height="27px" viewBox="0 0 24 27" style="enable-background:new 0 0 24 27;" xml:space="preserve">
                                        <line stroke-width="2" fill="none" x1="12" y1="0" x2="12" y2="18"></line>
                                        <polyline stroke-width="1" fill="none" points="1,18 1,26 23,26 23,18"></polyline>
                                        <polyline stroke-width="1" fill="none" points="6,11 12.5,17.5 18.7,11.3"></polyline>
                                    </svg>
                                </a>
                            <?php } ?>

                            <a class="arrow" href="<?php echo get_permalink($postId); ?>">
                                <svg x="0px" y="0px" width="25.623px" height="25.623px" viewBox="0 0 25.623 25.623" enable-background="new 0 0 25.623 25.623" xml:space="preserve">
                                    <circle fill="none" stroke-miterlimit="10" cx="12.811" cy="12.812" r="12.311"/>
                                    <line fill="none" stroke-miterlimit="10" x1="19.709" y1="13.051" x2="4.709" y2="13.051"/>
                                    <polyline fill="none" stroke-miterlimit="10" points="12.948,5.876 19.454,12.635 12.949,19.349 "/>
                                </svg>
                            </a>
                        </div>
                    </div>

                    <?php
                        endwhile;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
