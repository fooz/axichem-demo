<?php
$widget_name = "toparticle";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-toparticle so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrump_container">
                    <a class="text15" href="#">
                        <span class="text text15">axichem.se</span>
                        <svg class="arrow" x="0px" y="0px" width="11.2px" height="9.2px" viewBox="0 0 11.2 9.2" style="enable-background:new 0 0 11.2 9.2;" xml:space="preserve">
                        	<polyline points="6.4,8.8 10.5,4.5 6.5,0.3 	"/>
                        	<line x1="10.6" y1="4.5" x2="0" y2="4.5"/>
                        </svg>
                    </a>
                    <?php
                    $cat = wp_get_post_terms( get_the_ID(), 'type');
                    $term = get_term($cat[0]->term_id, 'type');
                    ?>
                    <a class="text15" href="<?php echo get_permalink(icl_object_id(765)); ?>?cat=<?php echo $cat[0]->slug; ?>">
                        <span class="text text15"><?php echo $cat[0]->name; ?></span>
                        <svg class="arrow" x="0px" y="0px" width="11.2px" height="9.2px" viewBox="0 0 11.2 9.2" style="enable-background:new 0 0 11.2 9.2;" xml:space="preserve">
                        	<polyline points="6.4,8.8 10.5,4.5 6.5,0.3 	"/>
                        	<line x1="10.6" y1="4.5" x2="0" y2="4.5"/>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="title text38"><?php echo get_the_title(); ?></div>
            </div>
            <div class="col-12">
                <div class="nav">
                    <a class="back" href="javascript: history.go(-1)">
                        <svg x="0px" y="0px" width="18.7px" height="15.2px" viewBox="0 0 18.7 15.2" style="enable-background:new 0 0 18.7 15.2;" xml:space="preserve">
                        	<polyline class="st0" points="8,0 1.1,7.2 7.8,14.2 	"/>
                        	<line class="st0" x1="0.9" y1="7.2" x2="18.7" y2="7.2"/>
                        </svg>
                        <span class="text text15"><?php _e('Back','axichem'); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
