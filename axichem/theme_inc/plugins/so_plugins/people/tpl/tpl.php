<?php
$widget_name = "people";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-people so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row header">
            <div class="col-12 col-md-4">
                <div class="text1 text15"><?php echo $instance['text1']; ?></div>
            </div>
            <div class="col-12 col-md-6 offset-md-1">
                <div class="text2 text38"><?php echo $instance['text2']; ?></div>
            </div>
        </div>



        <div class="people">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="category text15">Board of Directors</div>
                </div>
            </div>

            <div class="list">
                <div class="item row">
                <?php
                $queryOfferAll = new WP_Query(
                    array(
                        'post_type' => 'people',
                        'posts_per_page' => 6,
                        'order' => "DESC",
                        'orderby' => 'date',
                        'ignore_sticky_posts' => 1,
                        'post_status' => 'publish'
                    )
                );
                $x = 1;
                while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
                ?>

                <div class="col-12 col-md-4 person">
                    <div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large', true)[0]; ?>)"></div>
                    <div class="desc">
                        <div class="title text22" href=""><?php echo get_the_title(); ?></div>
                        <div class="description text15" href=""><?php echo get_the_content(); ?></div>
                    </div>
                </div>

                <?php
                if($x%3 == 0)
                    echo '</div><div class="item row">';
                ?>

                <?php
                    $x++;
                endwhile;
                wp_reset_query();
                ?>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="category text15">Management Team</div>
                </div>
            </div>

            <div class="list">
                <div class="item row">
                <?php
                $queryOfferAll = new WP_Query(
                    array(
                        'post_type' => 'people',
                        'posts_per_page' => 3,
                        'order' => "DESC",
                        'orderby' => 'date',
                        'ignore_sticky_posts' => 1,
                        'post_status' => 'publish'
                    )
                );
                $x = 1;
                while ($queryOfferAll->have_posts()) : $queryOfferAll->the_post();
                ?>

                <div class="col-12 col-md-4 person">
                    <div class="photo" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large', true)[0]; ?>)"></div>
                    <div class="desc">
                        <div class="title text22" href=""><?php echo get_the_title(); ?></div>
                        <div class="description text15" href=""><?php echo get_the_content(); ?></div>
                    </div>
                </div>

                <?php
                if($x%3 == 0)
                    echo '</div><div class="item row">';
                ?>

                <?php
                    $x++;
                endwhile;
                wp_reset_query();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
