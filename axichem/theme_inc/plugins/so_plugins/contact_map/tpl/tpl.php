<?php
$widget_name = "contactmap";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=<?php echo $instance['map_api']; ?>'></script>

<div id="<?php echo $widget_id; ?>" class="widget widget-contactmap so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div id="maps" data-param="<?php echo $instance['map'];?>" data-api="<?php echo $instance['map_api'];?>"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-5">
                <div class="header text38"><?php echo $instance['header'];?></div>
                <div class="text text18"><?php echo $instance['text'];?></div>
            </div>
        </div>
    </div>
</div>
