var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {

    if (jQuery('.widget-history').length == 1) {

        function timelineSize() {
            hMax = 0;
            jQuery('.widget-history .events .event').each(function() {
                if (jQuery(this).height() > hMax)
                    hMax = jQuery(this).height();
            })
            jQuery('.widget-history .events').height(hMax);
        }
        timelineSize();


        function timelineAnim() {
            //wScroll = jQuery('.widget-history .timeline .scroll').width();
            wYear = jQuery('.widget-history .timeline .year').width();
            wTimeline = wYear * jQuery('.widget-history .timeline .year').length
            jQuery('.widget-history .timeline .scroll').width(wTimeline)

            jQuery('.widget-history .timeline').removeClass('start');

            year = jQuery('.widget-history .timeline .year.active').data('year');
            i = jQuery('.widget-history .timeline .year.active').index();
            p = i / (jQuery('.widget-history .timeline .year').length - 1);


            if ($(window).width() > 767)
                width = (jQuery('.widget-history .timeline_container .scroll').width()) * p;
            else
                width = (jQuery('.widget-history .timeline_container .scroll').width() - wYear) * p;
            jQuery('.widget-history .timeline .timeline_container .line').width(width);
        }
        timelineAnim();


        jQuery(document).on(clickHandler, '.widget-history .timeline .year .bt', function(event) {
            year = jQuery(this).closest('.year').data('year');
            jQuery('.widget-history .timeline .year').removeClass('active');
            jQuery('.widget-history .timeline .year[data-year="' + year + '"]').addClass('active');

            jQuery('.widget-history .events .event').removeClass('active');
            jQuery('.widget-history .events .event[data-year="' + year + '"]').addClass('active');

            timelineAnim();
        });



        jQuery(document).on(clickHandler, '.widget-history .timeline .nav', function(event) {
            i = jQuery('.widget-history .timeline .timeline_container .year.active').index()
            iMax = jQuery('.widget-history .timeline .timeline_container .year').length - 1;

            if (jQuery(this).hasClass('prev'))
                i--;
            if (jQuery(this).hasClass('next'))
                i++;
            if (i < 0)

                i = 0;
            if (i > iMax)
                i = iMax;

            year = jQuery('.widget-history .timeline .year').eq(i).data('year');
            jQuery('.widget-history .timeline .year').removeClass('active');
            jQuery('.widget-history .timeline .year[data-year="' + year + '"]').addClass('active');

            jQuery('.widget-history .events .event').removeClass('active');
            jQuery('.widget-history .events .event[data-year="' + year + '"]').addClass('active');

            timelineAnim();

        })



        jQuery(window).resize(function() {
            timelineAnim();
            timelineSize();
        })
    }
});
