var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function articleOthersSize() {
        hArray = {};
        p = hMax = 0;

        jQuery('.widget-articleothers .box .title').height('auto');
        jQuery('.widget-articleothers .box').each(function () {
            if(p != jQuery(this).closest('.col').position().top)
                hArray[p] = hMax;

            p = jQuery(this).closest('.col').position().top;
            jQuery(this).attr('data-p', p);

            if(hMax < jQuery(this).find('.title').outerHeight())
                hMax = jQuery(this).find('.title').outerHeight();
        })
        hArray[p] = hMax;

        jQuery.each(hArray, function (keyFeature, valueFeature) {
            t = jQuery('.widget-articleothers .box[data-p="'+ keyFeature +'"] .title');
            t.outerHeight(valueFeature)
		})
    }
    articleOthersSize();
    setTimeout(function(){ articleOthersSize(); }, 1000);

    jQuery(window).resize(function() {
        articleOthersSize()
    })
});
