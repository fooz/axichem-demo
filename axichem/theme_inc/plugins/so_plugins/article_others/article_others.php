<?php

/*
Widget Name: Axichem
Description:
Author:
*/


class ArticleOthers_Widget extends SiteOrigin_Widget {

	const SO_PLUGIN_SLUG = 'article_others';
	const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/article_others/';
	const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/article_others/';
	const SO_PLUGIN_VER =  1;

	function __construct() {

		$form_options = array(

		);
		add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'article_others',
				__('Articles Others', 'axichem'),
				array(
						'description' => __('description', 'axichem'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/article_others/'
				);
		}

		function initialize(){
			wp_enqueue_style(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/style.css');
			wp_enqueue_script(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'));
	    }
		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('ArticleOthers Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/article_others/', 'ArticleOthers_Widget');
?>
