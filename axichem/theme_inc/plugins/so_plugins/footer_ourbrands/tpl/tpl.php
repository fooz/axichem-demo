<?php
$widget_name = "footer_ourbrands";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-footerourbrands so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
   <div class="container">
     	<div class="row">
         <div class="col-12 col-md-12"><?php __('Our Brands','axichem') ?></div>
            <?php
            $class = array(
                1 => "col-12 col-md-12",
                2 => "col-12 col-md-6",
                3 => "col-12 col-md-4",
                4 => "col-12 col-md-3",
                5 => "col-12 col-md-2",
                6 => "col-12 col-md-2",
                7 => "col-12 col-md-2",
                8 => "col-12 col-md-2",
            );


            foreach ($instance['brands'] as $key => $value) {
                ?>

               <div class="<?php echo $class[sizeof($instance['brands'])] ; ?>">
                 	<?php
            		$href = '#';
		          	if(strlen($instance['url']) > 0){
		               if(strpos($instance['url'], 'post:') === false){
		                   $href = $instance['url'];
		               } else {
		                   $hreflId = str_replace('post: ','', $instance['url']);
		                   $href = get_permalink($hreflId);
		               }
		           	}
            		?>
                    <a class="brand" href="<?php echo $href; ?>">
							  	<?php if(strlen($value['logo']) > 0 && $value['logo'] != 0){ ?>
						         <div class="logo">
										<img src="<?php echo wp_get_attachment_image_src($value['logo'], 'large', true)[0]; ?>" alt="<?php echo $value['title']; ?>" />
						         </div>
						     	<?php }else{ ?>
                        	<div class="title text26"><?php echo $value['title']; ?></div>
								<?php } ?>
                        <div class="text text15"><?php echo $value['text']; ?></div>
                        <div class="arrow">
                           <svg width="18.694px" height="15.225px" viewBox="0 0 18.694 15.225" enable-background="new 0 0 18.694 15.225" xml:space="preserve">
                           	<polyline fill="none" stroke-miterlimit="10" points="10.669,14.23 17.606,7.013 10.869,0"/>
                            	<line fill="none" stroke-miterlimit="10" x1="17.799" y1="7.024" x2="0" y2="7.024"/>
                           </svg>
                        </div>
                    </a>
                </div>

                <?php
            }
            ?>
        </div>
    </div>
</div>
