
<?php

/*
Widget Name: Axichem
Description:
Author:
*/


class Block_TextsAndBackgrounds_Widget extends SiteOrigin_Widget {

	const SO_PLUGIN_SLUG = 'block_texts_and_backgrounds';
	const SO_PLUGIN_DIR =  THEME_PLUGIN_DIRECTORY . 'so_plugins/block_texts_and_backgrounds/';
	const SO_PLUGIN_URL =  THEME_PLUGIN_DIRECTORY_URL . 'so_plugins/block_texts_and_backgrounds/';
	const SO_PLUGIN_VER =  1;

	function __construct() {

		$form_options = array(
			'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<h2>Options</h2><hr />'
            ),
			'uniq_id' => array(
			 	'type' => 'text',
			    'label' => __('Uniq Id', 'axichem'),
			    'default' => ''
			),
			'html'.(rand()*100) => array(
				'type' => 'html',
			    'default' => '<br /><img src="'. self::SO_PLUGIN_URL .'screen/main1.jpg" />'
			),
			'side_param' => array(
                'type' => 'selectmultiple',
                'real_label' => __('Left Column<br/>(Offset left)|Left Column<br/>(Width)|Left Column<br/>(Offset right)|Right Column<br/>(Offset left)|Right Column<br/>(Width)|Right Column<br/>(Offset right)', 'axichem'),
                'real_var' => array(
					'sideleft_offsetleft' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
					'sideleft_width' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
					'sideleft_offsetright' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
					'sideright_offsetleft' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
					'sideright_width' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
					'sideright_offsetright' => array(
						'0' => 'none',
						'1' => 'col-1',
						'2' => 'col-2',
						'3' => 'col-3',
						'4' => 'col-4',
						'5' => 'col-5',
						'6' => 'col-6',
						'7' => 'col-7',
						'8' => 'col-8',
						'9' => 'col-9',
						'10' => 'col-10',
						'11' => 'col-11',
						'12' => 'col-12'
					),
				),
                'description' => __('', 'axichem'),
                'default' => ''
            ),
            'align_vertical' => array(
                'type' => 'checkbox',
                'label' => __( 'Center inside', 'axichem' ),
                'default' => false
            ),

			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<br /><img src="'. self::SO_PLUGIN_URL .'screen/main2.jpg" />'
			),
			'side_background' => array(
                'type' => 'media',
                'label' => __('Background - add photo ', 'axichem'),
                'choose' => __('Select', 'axichem'),
                'update' => __('Set', 'axichem'),
                'library' => 'image',
                'fallback' => true
            ),
            'side_background_color' => array(
                'type' => 'color',
                'label' => __('Background - color', 'axichem'),
                'default' => ''
            ),


			// Left side
			// ======================================================================
			//
			'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><br /><h1>Left side</h1><hr />'
            ),
			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<img src="'. self::SO_PLUGIN_URL .'screen/left1.jpg" />'
			),
			'sideleft_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom)', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),


			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<br /><img src="'. self::SO_PLUGIN_URL .'screen/left2.jpg" />'
			),
			'sideleft_background' => array(
                'type' => 'media',
                'label' => __('Background - add photo ', 'axichem'),
                'choose' => __('Select', 'axichem'),
                'update' => __('Set', 'axichem'),
                'library' => 'image',
                'fallback' => true
            ),
            'sideleft_background_color' => array(
                'type' => 'color',
                'label' => __('Background - color', 'axichem'),
                'default' => ''
            ),
			'sideleft_background_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
			'sideleft_background_scale' => array(
                'type' => 'checkbox',
                'label' => __('Background - fullscreen ', 'engine'),
                'default' => false
            ),
			'sideleft_background_first' => array(
                'type' => 'checkbox',
                'label' => __( 'First element', 'axichem' ),
                'default' => false
            ),


			'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><h3>Content</h3><br />'
            ),

			'sideleft_header' => array(
				'type' => 'tinymce',
			   	'label' => __('Header', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'sideleft_header_color' => array(
                'type' => 'color',
                'label' => __('Header color', 'axichem'),
                'default' => ''
            ),
            'sideleft_text' => array(
                'type' => 'tinymce',
			   	'label' => __('Paragraph', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'sideleft_text_color' => array(
                'type' => 'color',
                'label' => __('Paragraph color', 'axichem'),
                'default' => ''
            ),


			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<br /><img src="'. self::SO_PLUGIN_URL .'screen/left3.jpg" />'
			),
            'sideleft_text_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),

            'sideleft_link_text' => array(
                'type' => 'text',
                'label' => __('Link - text', 'axichem'),
                'default' => ''
            ),
            'sideleft_link_url' => array(
                'type' => 'link',
                'label' => __('Link - url', 'axichem'),
                'default' => ''
            ),
			'sideleft_link_format' => array(
                'type' => 'select',
                'label' => __('Link - format', 'axichem'),
                'default' => 'text',
                'options' => array(
                    'link' => __('text', 'axichem'),
                    'button' => __('button', 'axichem'),
                )
            ),
			'sideleft_link_newwindow' => array(
		        'type' => 'checkbox',
		        'label' => __('Open new window', 'axichem' ),
		        'default' => false
		    ),
			//
			// Right side
			// ======================================================================
			//
			'html'.(rand()*100) => array(
                'type' => 'html',
                'default' => '<br /><br /><br /><h2>Right side</h2><hr />'
            ),
			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<img src="'. self::SO_PLUGIN_URL .'screen/right1.jpg" />'
			),
			'sideright_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom)', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),


			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<img src="'. self::SO_PLUGIN_URL .'screen/right2.jpg" />'
			),
			'sideright_background' => array(
                'type' => 'media',
                'label' => __('Background - add photo ', 'axichem'),
                'choose' => __('Select', 'axichem'),
                'update' => __('Set', 'axichem'),
                'library' => 'image',
                'fallback' => true
            ),
            'sideright_background_color' => array(
                'type' => 'color',
                'label' => __('Background - color', 'axichem'),
                'default' => ''
            ),
			'sideright_background_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),
			'sideright_background_scale' => array(
                'type' => 'checkbox',
                'label' => __('Background - fullscreen ', 'engine'),
                'default' => false
            ),
			'sideright_background_first' => array(
                'type' => 'checkbox',
                'label' => __( 'First element', 'axichem' ),
                'default' => false
            ),

			'sideright_header' => array(
				'type' => 'tinymce',
			   	'label' => __('Header', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'sideright_header_color' => array(
                'type' => 'color',
                'label' => __('Header color', 'axichem'),
                'default' => ''
            ),
            'sideright_text' => array(
                'type' => 'tinymce',
			   	'label' => __('Paragraph', 'axichem'),
 			  	'default' => '',
			   	'rows' => 5,
			   	'default_editor' => 'html',
			   	'button_filters' => array(
					'mce_buttons' => array($this, 'filter_mce_buttons'),
				   	'mce_buttons_2' => array($this, 'filter_mce_buttons_2'),
				   	'mce_buttons_3' => array($this, 'filter_mce_buttons_3'),
				   	'mce_buttons_4' => array($this, 'filter_mce_buttons_5'),
				   	'quicktags_settings' => array($this, 'filter_quicktags_settings'),
			   	),
            ),
            'sideright_text_color' => array(
                'type' => 'color',
                'label' => __('Paragraph color', 'axichem'),
                'default' => ''
            ),

			'html'.(rand()*1000) => array(
				'type' => 'html',
				'default' => '<img src="'. self::SO_PLUGIN_URL .'screen/right3.jpg" />'
			),
            'sideright_text_pad' => array(
                'type' => 'inputmultiple',
                'real_label' => __('FullHD<br/>(padding top/bottom)|Pc<br/>(padding top/bottom)|Tablet<br/>(padding top/bottom)|Phone<br/>(padding top/bottom', 'axichem'),
                'real_var' => __('padding_fullhd|padding_pc|padding_tablet|padding_phone', 'axichem'),
                'description' => __('Top Bottom, ex. "20 20"', 'axichem'),
                'default' => '0 0'
            ),

            'sideright_link_text' => array(
                'type' => 'text',
                'label' => __('Link - text', 'axichem'),
                'default' => ''
            ),
            'sideright_link_url' => array(
                'type' => 'link',
                'label' => __('Link - url', 'axichem'),
                'default' => ''
            ),
			'sideright_link_format' => array(
                'type' => 'select',
                'label' => __('Link - format', 'axichem'),
                'default' => 'text',
                'options' => array(
                    'link' => __('text', 'axichem'),
                    'button' => __('button', 'axichem'),
                )
            ),
			'sideright_link_newwindow' => array(
		        'type' => 'checkbox',
		        'label' => __('Open new window', 'axichem' ),
		        'default' => false
		    )






		);
		add_filter( 'siteorigin_widgets_sanitize_field_date', array( $this, 'sanitize_date' ) );

		parent::__construct(
				'block_texts_and_backgrounds',
				__('Block: Texts & Two Backgrounds ', 'axichem'),
				array(
						'description' => __('description', 'axichem'),
						'help'        => TEMPL_OWNER_URL,
						'panels_groups' => array(TEMPL_NAME),
						'panels_icon' => 'dashicons dashicons-admin-settings'
				),
				array(
				),
				$form_options,
				THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_backgrounds/'
				);
		}

		function initialize(){
			wp_enqueue_style(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/style.css');
			wp_enqueue_script(self::SO_PLUGIN_SLUG, self::SO_PLUGIN_URL . 'tpl/script.js', array('jquery'));
	    }
		public function widget( $args, $instance ) {
			include('tpl/tpl.php');
		}
		function get_template_name($instance) {
				return 'tpl';
		}

		function get_style_name($instance) {
				return '';
		}

		function filter_mce_buttons( $buttons, $editor_id ) {
		if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
				 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
			unset($buttons[$key]);
		}
		return $buttons;
		}
		function filter_mce_buttons_2( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_3( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		function filter_mce_buttons_4( $buttons, $editor_id ) {
			if ( ( $key = array_search( 'fullscreen', $buttons ) ) !== false ||
					 ( $key = array_search( 'dfw', $buttons ) ) !== false) {
				unset($buttons[$key]);
			}
			return $buttons;
		}
		public function quicktags_settings( $settings, $editor_id ) {
			$settings['buttons'] = preg_replace( '/,fullscreen/', '', $settings['buttons'] );
			$settings['buttons'] = preg_replace( '/,dfw/', '', $settings['buttons'] );
			return $settings;
		}
		function sanitize_date( $date_to_sanitize ) {
			// Perform custom date sanitization here.
			$sanitized_date = sanitize_text_field( $date_to_sanitize );
			return $sanitized_date;
		}


}
siteorigin_widget_register('Block_TextsAndBackgrounds Widget', THEME_PLUGIN_DIRECTORY.'so_plugins/block_texts_and_backgrounds/', 'Block_TextsAndBackgrounds_Widget');
?>
