<?php
$widget_name = "widget-block_texts_and_backgrounds";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<?php
// Side setup
// ================================

$sidewidthArray = array(
    "1" => "col-12 col-md-1 col-lg-1",
    "2" => "col-12 col-md-2 col-lg-2",
    "3" => "col-12 col-md-3 col-lg-3",
    "4" => "col-12 col-md-4 col-lg-4",
    "5" => "col-12 col-md-5 col-lg-5",
    "6" => "col-12 col-md-6 col-lg-6",
    "7" => "col-12 col-md-7 col-lg-7",
    "8" => "col-12 col-md-8 col-lg-8",
    "9" => "col-12 col-md-9 col-lg-9",
    "10" => "col-12 col-md-10 col-lg-10",
    "11" => "col-12 col-md-11 col-lg-11",
    "12" => "col-12 col-md-12 col-lg-12"
);
$sidewidthLeftOffsetArray = array(
    "1" => "offset-md-1",
    "2" => "offset-md-2",
    "3" => "offset-md-3",
    "4" => "offset-md-4",
    "5" => "offset-md-5",
    "6" => "offset-md-6",
    "7" => "offset-md-7",
    "8" => "offset-md-8",
    "9" => "offset-md-9",
    "10" => "offset-md-10",
    "11" => "offset-md-11",
    "12" => "offset-md-12"
);
$sidewidthRightOffsetArray = array(
    "1" => "offset-right-md-1",
    "2" => "offset-right-md-2",
    "3" => "offset-right-md-3",
    "4" => "offset-right-md-4",
    "5" => "offset-right-md-5",
    "6" => "offset-right-md-6",
    "7" => "offset-right-md-7",
    "8" => "offset-right-md-8",
    "9" => "offset-right-md-9",
    "10" => "offset-right-md-10",
    "11" => "offset-right-md-11",
    "12" => "offset-right-md-12",
);



$side_param = json_decode($instance['side_param'], true);
?>

<a rel="<?php echo $instance['uniq_id']; ?>"></a>
<div id="<?php echo $widget_id; ?>" class="widget widget-block_texts_and_backgrounds so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-center="<?php echo $instance['align_vertical']; ?>">
    <div class="container">
        <div class="row">
            <?php
            // Column left
            // ================================

            $sideleft_padDecode = json_decode($instance['sideleft_pad'], true);
            $side1Style = '{"fullhd":"padding:'. $sideleft_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideleft_padDecode['padding_pc'] .'","tablet":"padding:'. $sideleft_padDecode['padding_tablet'] .'","phone":"padding:'. $sideleft_padDecode['padding_phone'] .'"}';

            $side1content = '<div class="inside" ';
            $side1content .= "data-style='". $side1Style ."'>";
            $side1content .= '<div class="inside_relative">';

            $colorHeader = $colorText = '';
            if($instance['sideleft_text_color'])
               $colorHeader = 'color: '. $instance['sideleft_header_color'] .';';
            if($instance['sideleft_text_color'])
               $colorText = 'color: '. $instance['sideleft_text_color'] .';';

            if(strlen($instance['sideleft_header']) > 0){
					$sideleft_header = $instance['sideleft_header'];
					$sideleft_header = preg_replace('/<h1>/i', '<h1 class="text38">', $sideleft_header);
					$sideleft_header = preg_replace('/<h2>/i', '<h2 class="text38">', $sideleft_header);
					$sideleft_header = preg_replace('/<h3>/i', '<h3 class="text38">', $sideleft_header);
					$sideleft_header = preg_replace('/<h4>/i', '<h4 class="text24">', $sideleft_header);
					$sideleft_header = preg_replace('/<h5>/i', '<h5 class="text24">', $sideleft_header);
					$sideleft_header = preg_replace('/<h6>/i', '<h6 class="text24">', $sideleft_header);

					$side1content .= '<div class="header text15" style="'. $colorHeader .'">'. $sideleft_header .'</div>';
            }
            if(strlen($instance['sideleft_text']) > 0){

					$style1TextJson = json_decode($instance['sideleft_text_pad'], true);
					$style1TextStr = '{"fullhd":"padding:'. $style1TextJson['padding_fullhd'] .'","pc":"padding:'. $style1TextJson['padding_pc'] .'","tablet":"padding:'. $style1TextJson['padding_tablet'] .'","phone":"padding:'. $style1TextJson['padding_phone'] .'"}';
					$style1Text .= "data-style='". $style1TextStr ."'";

					$sideleft_text = $instance['sideleft_text'];
					$sideleft_text = preg_replace('/<h1>/i', '<h1 class="text38">', $sideleft_text);
					$sideleft_text = preg_replace('/<h2>/i', '<h2 class="text38">', $sideleft_text);
					$sideleft_text = preg_replace('/<h3>/i', '<h3 class="text38">', $sideleft_text);
					$sideleft_text = preg_replace('/<h4>/i', '<h4 class="text24">', $sideleft_text);
					$sideleft_text = preg_replace('/<h5>/i', '<h5 class="text24">', $sideleft_text);
					$sideleft_text = preg_replace('/<h6>/i', '<h6 class="text24">', $sideleft_text);
					$sideleft_text = preg_replace('/<p>/i', '<p class="text22">', $sideleft_text);

					//$side1content .= '<div class="text" '. $style1Text .'><div style="'. $colorText .'">'. do_shortcode(nl2br($sideleft_text)) .'</div></div>';
					$side1content .= '<div class="text" '. $style1Text .'><div style="'. $colorText .'">'. do_shortcode($sideleft_text) .'</div></div>';
            }
            if(strlen($instance['sideleft_link_text']) > 0 && strlen($instance['sideleft_link_url']) > 0){
                $href = '#';
                if(strlen($instance['sideleft_link_url']) > 0){
                    if(strpos($instance['sideleft_link_url'], 'post:') === false){
                        $href = $instance['sideleft_link_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['sideleft_link_url']);
                        $href = get_permalink($hreflId);
                    }
                }
                $target = '';
                if($instance['sideleft_link_newwindow'] == 1)
                    $target = 'target="_blank"';

                $side1content .= '<a class="'. $instance['sideleft_link_format'] .' text15" href="'. $href .'" '. $target .'>'. $instance['sideleft_link_text'] .'</a>';
            }

            $background_image = $background_color = $fullscreen = $background_class = '';
            if(strlen($instance['sideleft_background']) > 1){
                $background_class = 'image';
                $background_image = 'background-image: url('. wp_get_attachment_image_src($instance['sideleft_background'], 'large', true)[0] .'); ';
            }

            if(strlen($instance['sideleft_background_color']) > 1)
                $background_color = 'background-color: '. $instance['sideleft_background_color'] . ';';
            $fullscreen = '';
            if($instance['sideleft_background_scale'] == 1)
                $fullscreen = 'fullscreen';


            $sideleft_background_padDecode = json_decode($instance['sideleft_background_pad'], true);
            $sideleft_background_padStyle = '{"fullhd":"padding:'. $sideleft_background_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideleft_background_padDecode['padding_pc'] .'","tablet":"padding:'. $sideleft_background_padDecode['padding_tablet'] .'","phone":"padding:'. $sideleft_background_padDecode['padding_phone'] .'","other":"'. $background_image .' '. $background_color .'"}';
            $sideleft_background_padStyle = "data-style='".$sideleft_background_padStyle."'";

            $side1content .= '<div class="background '. $fullscreen .' '. $background_class .'" '. $sideleft_background_padStyle .'"></div>';
            $side1content .= '</div></div>';
            ?>
            <div class="col col-left <?php echo $sidewidthArray[$side_param['sideleft_width']]; ?> <?php echo $sidewidthLeftOffsetArray[$side_param['sideleft_offsetleft']]; ?> <?php echo $sidewidthRightOffsetArray[$side_param['sideleft_offsetright']]; ?>" data-order="<?php echo $instance['sideleft_background_first']; ?>"><?php echo do_shortcode($side1content); ?></div>



            <?php
            // Column right
            // ================================

            $sideright_padDecode = json_decode($instance['sideright_pad'], true);
            $side2Style = '{"fullhd":"padding:'. $sideright_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideright_padDecode['padding_pc'] .'","tablet":"padding:'. $sideright_padDecode['padding_tablet'] .'","phone":"padding:'. $sideright_padDecode['padding_phone'] .'"}';

            $side2content = '<div class="inside" ';
            $side2content .= "data-style='". $side2Style ."'>";
            $side2content .= '<div class="inside_relative">';

            $colorHeader = $colorText = '';
            if($instance['sideright_header_color'])
                $colorHeader = 'color: '. $instance['sideright_header_color'] .';';
            if($instance['sideright_text_color'])
                $colorText = 'color: '. $instance['sideright_text_color'] .';';

            if(strlen($instance['sideright_header']) > 0){
                $sideright_header = $instance['sideright_header'];
                $sideright_header = preg_replace('/<h1>/i', '<h1 class="text38">', $sideright_header);
                $sideright_header = preg_replace('/<h2>/i', '<h2 class="text38">', $sideright_header);
                $sideright_header = preg_replace('/<h3>/i', '<h3 class="text38">', $sideright_header);
                $sideright_header = preg_replace('/<h4>/i', '<h4 class="text24">', $sideright_header);
                $sideright_header = preg_replace('/<h5>/i', '<h5 class="text24">', $sideright_header);
                $sideright_header = preg_replace('/<h6>/i', '<h6 class="text24">', $sideright_header);

                $side2content .= '<div class="header text15" style="'. $colorHeader .'">'. $sideright_header .'</div>';
            }
            if(strlen($instance['sideright_text']) > 0){

                $style2TextJson = json_decode($instance['sideright_text_pad'], true);
                $style2TextStr = '{"fullhd":"padding:'. $style2TextJson['padding_fullhd'] .'","pc":"padding:'. $style2TextJson['padding_pc'] .'","tablet":"padding:'. $style2TextJson['padding_tablet'] .'","phone":"padding:'. $style2TextJson['padding_phone'] .'"}';
                $style2Text .= "data-style='". $style2TextStr ."'";

                $sideright_text = $instance['sideright_text'];
                $sideright_text = preg_replace('/<h1>/i', '<h1 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h2>/i', '<h2 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h3>/i', '<h3 class="text38">', $sideright_text);
                $sideright_text = preg_replace('/<h4>/i', '<h4 class="text24">', $sideright_text);
                $sideright_text = preg_replace('/<h5>/i', '<h5 class="text24">', $sideright_text);
                $sideright_text = preg_replace('/<h6>/i', '<h6 class="text24">', $sideright_text);
                $sideright_text = preg_replace('/<p>/i', '<p class="text22">', $sideright_text);

                $side2content .= '<div class="text" '. $style2Text .'><div style="'. $colorText .'">'. do_shortcode($sideright_text) .'</div></div>';
            }
            if(strlen($instance['sideright_link_text']) > 0 && strlen($instance['sideright_link_url']) > 0){
                $href = '#';
                if(strlen($instance['sideright_link_url']) > 0){
                    if(strpos($instance['sideright_link_url'], 'post:') === false){
                        $href = $instance['sideright_link_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['sideright_link_url']);
                        $href = get_permalink($hreflId);
                    }
                }
                $target = '';
                if($instance['sideright_link_newwindow'] == 1)
                    $target = 'target="_blank"';

                $side2content .= '<a class="'. $instance['sideright_link_format'] .' text15" href="'. $href .'" '. $target .'>'. $instance['sideright_link_text'] .'</a>';
            }

            $background_image = $background_color = $fullscreen = $background_class = '';
            if(strlen($instance['sideright_background']) > 1){
                $background_class = 'image';
                $background_image = 'background-image: url('. wp_get_attachment_image_src($instance['sideright_background'], 'large', true)[0] .'); ';
            }
            if(strlen($instance['sideright_background_color']) > 1)
                $background_color = 'background-color: '. $instance['sideright_background_color'] . ';';
            $fullscreen = '';
            if($instance['sideright_background_scale'] == 1)
                $fullscreen = 'fullscreen';

            $sideright_background_padDecode = json_decode($instance['sideright_background_pad'], true);
            $sideright_background_padStyle = '{"fullhd":"padding:'. $sideright_background_padDecode['padding_fullhd'] .'","pc":"padding:'. $sideright_background_padDecode['padding_pc'] .'","tablet":"padding:'. $sideright_background_padDecode['padding_tablet'] .'","phone":"padding:'. $sideright_background_padDecode['padding_phone'] .'","other":"'. $background_image .' '. $background_color .'"}';
            $sideright_background_padStyle = "data-style='".$sideright_background_padStyle."'";

            $side2content .= '<div class="background '. $fullscreen .' '. $background_class .'" '.$sideright_background_padStyle.'></div>';
            $side2content .= '</div></div>';
            ?>
            <div class="col col-right <?php echo $sidewidthArray[$side_param['sideright_width']]; ?> <?php echo $sidewidthLeftOffsetArray[$side_param['sideright_offsetleft']]; ?> <?php echo $sidewidthRightOffsetArray[$side_param['sideright_offsetright']]; ?>" data-order="<?php echo $instance['sideright_background_first']; ?>"><?php echo do_shortcode($side2content); ?></div>

            <?php
            if(strlen($instance['side_background']) > 1)
                $backgroundglobal_image = 'background-image: url('. wp_get_attachment_image_src($instance['side_background'], 'large', true)[0] .'); ';
            if(strlen($instance['side_background_color']) > 1)
                $backgroundglobal_color = 'background-color: '. $instance['side_background_color'] . ';';

            echo '<div class="background_global" style="'. $backgroundglobal_image .' '. $backgroundglobal_color . '"></div>';
            ?>
        </div>


    </div>
</div>
