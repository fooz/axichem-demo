var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function blocktextsbackgroundsSize() {
        jQuery('.widget-block_texts_and_backgrounds').each(function() {
            jQuery(this).find('.background').each(function() {
                if (jQuery(this).hasClass('fullscreen')) {
                    bodyWidth = jQuery(window).width();
                    containerWidth = jQuery(this).closest('.container').outerWidth();
                    colWidth = jQuery(this).closest('.col').outerWidth(true);

                    offsetLeft = parseInt(jQuery(this).closest('.col').css('margin-left'))
                    offsetRight = parseInt(jQuery(this).closest('.col').css('margin-right'))

                    if($(window).width() > 767){
                        width = Math.ceil(Math.ceil((bodyWidth - containerWidth) / 2) + colWidth);

                        jQuery(this).css({
                            'width': width
                        })
                        if (jQuery(this).closest('.col').hasClass('col-left'))
                            jQuery(this).css({
                                'right': -offsetRight + 'px'
                            })
                        if (jQuery(this).closest('.col').hasClass('col-right'))
                            jQuery(this).css({
                                'left': -offsetLeft + 'px'
                            })
                    }

                    if($(window).width() > 576 && $(window).width() <= 767){
                        jQuery(this).css({
                            'width': '100vw',
                            'left': 'initial',
                            'right': 'initial'
                        })
                        jQuery(this).css({
                            'left': -(bodyWidth - colWidth)/2 - 15 + 'px'
                        })
                    }
                }
            })
        })
    }
    blocktextsbackgroundsSize();

    function blocktextsbackgroundsCenter() {
        jQuery('.widget-block_texts_and_backgrounds[data-center="1"]').each(function() {
            jQuery(this).find('.col-left .text').removeClass('centerH')
            jQuery(this).find('.col-right .text').removeClass('centerH')

            leftH = jQuery(this).find('.col-left .text').outerHeight()
            rightH = jQuery(this).find('.col-right .text').outerHeight()

            if(leftH != null && rightH != null){
                maxH = 0;
                if(leftH > maxH){
                    maxH = leftH
                }
                if(rightH > maxH){
                    maxH = rightH
                }

                if(leftH < rightH){
                    jQuery(this).find('.col-left .text').addClass('centerH')
                }else{
                    jQuery(this).find('.col-right .text').addClass('centerH')
                }

                jQuery(this).find('.inside_relative').outerHeight(maxH)

                jQuery(this).find('.col-left .button').css('bottom', parseInt(jQuery(this).find('.col-left .inside').css('padding-bottom')) );
                jQuery(this).find('.col-right .button').css('bottom', parseInt(jQuery(this).find('.col-right .inside').css('padding-bottom')) );
            }
        })
    }
    blocktextsbackgroundsCenter();

    jQuery(window).resize(function() {
        blocktextsbackgroundsSize();
        blocktextsbackgroundsCenter();
    })
});
