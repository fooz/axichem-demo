<?php
$widget_name = "block_text_and_photo";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<?php
$styleOther = '';
if(strlen($instance['background_color']) > 0){
    $styleOther .= 'background-color: '. $instance['background_color'] .';';
}

$background_margDecode = json_decode($instance['background_marg'], true);
$marginBackground = '{"fullhd":"padding:'. $background_margDecode['padding_fullhd'] .'","pc":"padding:'. $background_margDecode['padding_pc'] .'","tablet":"padding:'. $background_margDecode['padding_tablet'] .'","phone":"padding:'. $background_margDecode['padding_phone'] .'","other":"'. $styleOther .'"}';
?>
<div id="<?php echo $widget_id; ?>" class="widget widget-blocktextphoto so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" data-side="<?php echo $instance['side']; ?>" data-style='<?php echo $marginBackground; ?>'>
    <div class="container">
        <div class="row">
            <?php
            // Side setup
            // ================================

            $sidewidthArray = array(
                "1" => "col-12 col-md-1 col-lg-1",
                "2" => "col-12 col-md-2 col-lg-2",
                "3" => "col-12 col-md-3 col-lg-3",
                "4" => "col-12 col-md-4 col-lg-4",
                "5" => "col-12 col-md-5 col-lg-5",
                "6" => "col-12 col-md-6 col-lg-6",
                "7" => "col-12 col-md-7 col-lg-7",
                "8" => "col-12 col-md-8 col-lg-8",
                "9" => "col-12 col-md-9 col-lg-9",
                "10" => "col-12 col-md-10 col-lg-10",
                "11" => "col-12 col-md-11 col-lg-11",
            );
            //$sidewidth = explode('-', $instance['side_width']);

            if($instance['side'] == 'left'){
                $side1class = $sidewidthArray[$instance['side_width_left']];
                $side2class = $sidewidthArray[$instance['side_width_right']-1].' offset-md-1';
            }
            if($instance['side'] == 'right'){
                $side1class = $sidewidthArray[$instance['side_width_left']];
                $side2class = $sidewidthArray[$instance['side_width_right']-1].' offset-md-1';
            }


            // Background
            // ================================

            $background = '';
            if(strlen($instance['background']) > 0){
                $fullscreen = '';
                if($instance['background_scale'] == 1)
                    $fullscreen = 'fullscreen';
                $background .= '<div class="background '. $fullscreen .'" style="background-image: url('. wp_get_attachment_image_src($instance['background'], 'large', true)[0] .'); background-position: '. $instance['background_align'] .' center;"></div>';
            }


            // Inside
            // ================================

            $inside_padDecode = json_decode($instance['inside_pad'], true);
            $paddingInside = '{"fullhd":"padding:'. $inside_padDecode['padding_fullhd'] .'","pc":"padding:'. $inside_padDecode['padding_pc'] .'","tablet":"padding:'. $inside_padDecode['padding_tablet'] .'","phone":"padding:'. $inside_padDecode['padding_phone'] .'"}';

            $content = '<div class="inside" ';
            $content .= "data-style='" . $paddingInside . "'>";

            if(strlen($instance['inside_header']) > 0)
                $content .= '<div class="header text15">'. $instance['inside_header'] .'</div>';
            if(strlen($instance['inside_text']) > 0){
                $color = '';
                if($instance['inside_text_color'])
                    $color = 'color: '. $instance['inside_text_color'] .';';
                $content .= '<div class="text text24" style="'. $color .'">'. $instance['inside_text'] .'</div>';
            }
            if(strlen($instance['inside_some_link_text']) > 0 && strlen($instance['inside_some_link_url']) > 0){
                $href = '#';
                if(strlen($instance['inside_some_link_url']) > 0){
                    if(strpos($instance['inside_some_link_url'], 'post:') === false){
                        $href = $instance['inside_some_link_url'];
                    } else {
                        $hreflId = str_replace('post: ','', $instance['inside_some_link_url']);
                        $href = get_permalink($hreflId);
                    }
                }

                $content .= '<a class="link text15" href="'. $href .'">'. $instance['inside_some_link_text'] .'</a>';
            }
            $content .= '</div>';
            ?>


            <div class="col <?php echo ($instance['side'] == 'left') ? 'col-background' : 'col-content'; ?> <?php echo $side1class; ?>">
                <?php
                if($instance['side'] == 'left')
                    echo $background;

                if($instance['side'] == 'right')
                    echo $content;
                ?>
            </div>
            <div class="col <?php echo ($instance['side'] == 'left') ? 'col-content' : 'col-background'; ?> <?php echo $side2class; ?>">
                <?php
                if($instance['side'] == 'left')
                    echo $content;

                if($instance['side'] == 'right')
                    echo $background;
                ?>
            </div>
        </div>
    </div>
</div>
