<?php
$widget_name = "404";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-404 so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="title text38">Error 404</div>
            </div>
            <div class="col-12">
                <div class="nav">
                    <a class="back" href="<?php echo get_permalink(icl_object_id(6)); ?>">
                        <span class="text text15"><?php _e('Back to Home Page','axichem'); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
