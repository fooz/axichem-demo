<?php
$widget_name = "products";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-products so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">

        <div class="row description">
            <div class="col-12 col-sm-6 col-md-5">
                <div class="header text15"><?php echo $instance['header']; ?></div>
            </div>
            <div class="col-12 col-sm-6 col-md-7">
                <div class="text text38"><?php echo $instance['text']; ?></div>
            </div>
        </div>

        <div class="row products">
            <?php
            if(sizeof($instance['products']) > 0)
            foreach ($instance['products'] as $key => $value) {
                ?>

                <div class="col-12 col-md-6 col-lg-3">
                    <?php
                    $href = '#';
                    if(strlen($value['url']) > 0){
                        if(strpos($value['url'], 'post:') === false){
                            $href = $value['url'];
                        } else {
                            $hreflId = str_replace('post: ','', $value['url']);
                            $href = get_permalink($hreflId);
                        }
                    }
                    ?>
                    <a class="product" href="<?php echo $href; ?>">
                        <div class="photo" style="background-image: url('<?php echo wp_get_attachment_image_src($value['photo'], 'large', true)[0]; ?>');"></div>
                        <div class="desc">
                            <span class="title text22"><?php echo $value['title']; ?></span>
                            <div class="arrow">
                                <svg width="18.694px" height="15.225px" viewBox="0 0 18.694 15.225" enable-background="new 0 0 18.694 15.225" xml:space="preserve">
                                	<polyline fill="none" stroke-miterlimit="10" points="10.669,14.23 17.606,7.013 10.869,0"/>
                                	<line fill="none" stroke-miterlimit="10" x1="17.799" y1="7.024" x2="0" y2="7.024"/>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
