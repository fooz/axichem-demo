<?php
$widget_name = "pages";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];

$section_padDecode = json_decode($instance['section_pad'], true);
$section_padStyle = '{"fullhd":"padding:'. $section_padDecode['padding_fullhd'] .'","pc":"padding:'. $section_padDecode['padding_pc'] .'","tablet":"padding:'. $section_padDecode['padding_tablet'] .'","phone":"padding:'. $section_padDecode['padding_phone'] .'"}';

//$section_padStylePaste = "data-style='". $section_padStyle ."'";
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-pages so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>">
	    <div class="container">
	        <div class="row">
	            <div class="col-12 col-lg-3 col-menu">
	                <div class="links">
										<?php
										$actId = get_the_ID();
										$page = get_post(get_the_ID());
										$parentId = get_the_ID();
										$parentTitle = $childrenTitle = $page->post_title;
										$childrenContent = $page->post_content;

										if($page->post_parent != 0){
										    $page = get_post($page->post_parent);
										    $parentId = $page->ID;
										    $parentTitle = $page->post_title;
										}

										$queryPages = new WP_Query(
												array(
														'post_type' => 'page',
														'post_parent' => $parentId,
														'posts_per_page' => -1,
														'order' => "DESC",
														'orderby' => 'menu_order',
														'ignore_sticky_posts' => 1,
														'post_status' => 'publish'
												)
										);
										$x = 1;
										while ($queryPages->have_posts()) : $queryPages->the_post();
												$class = '';
												if($actId == get_the_ID())
														$class = 'active';
												echo '<a class="'. $class .'" href="'. get_permalink() .'">'. get_the_title() .'</a>';
										endwhile;
										wp_reset_query();
										?>
	                </div>
	            </div>
	            <div class="col-12 col-lg-7 offset-lg-1 col-content">
	                <div class="title_page text38"><?php echo get_the_title(); ?></div>
	                <div class="content text15">
	                    <?php
	                    $content = $instance['text'];
	                    $content = preg_replace('/<h1>/i', '<h1 class="text38">', $content);
	                    $content = preg_replace('/<h2>/i', '<h2 class="text38">', $content);
	                    $content = preg_replace('/<h3>/i', '<h3 class="text38">', $content);
	                    $content = preg_replace('/<h4>/i', '<h4 class="text32">', $content);
	                    $content = preg_replace('/<h5>/i', '<h5 class="text32">', $content);
	                    $content = preg_replace('/<h6>/i', '<h6 class="text32">', $content);
	                    echo do_shortcode($content);
	                    ?>
	                </div>
	            </div>
	        </div>
	    </div>

</div>
