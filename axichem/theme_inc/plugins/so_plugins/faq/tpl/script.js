var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {

    function faqTabsRender(){
        wSum = 0;
        wMin = jQuery('.widget-faq .tabs_container').width()
        jQuery('.widget-faq .tabs_container .tab').each(function() {
            wSum += jQuery(this).outerWidth() + 5;
        })
        if(wSum < wMin)
            wSum = wMin;
        jQuery('.widget-faq .tabs_container .scroll').outerWidth(wSum);
    }
    faqTabsRender();

    jQuery(document).on(clickHandler, '.widget-faq .tab', function(event) {
        key = jQuery(this).attr('data-key');

        jQuery('.widget-faq .tabs_container .tab').removeClass('active');
        jQuery('.widget-faq .tabs_container .tab[data-key='+ key +']').addClass('active');

        jQuery('.widget-faq .sections_container .sections_group').removeClass('active');
        jQuery('.widget-faq .sections_container .sections_group[data-key='+ key +']').addClass('active');
    })


    jQuery(document).on(clickHandler, '.widget-faq .sections_container .section .short .helper, .widget-faq .sections_container .section .arrow', function(event) {
        if (!jQuery(this).closest('.section').hasClass('active')) {
            h = jQuery(this).closest('.section').find('.description').outerHeight();

            if ($(window).width() < 767)
                h += jQuery(this).closest('.section').find('.topic').outerHeight();
        }else{
            h = 0;
        }
        jQuery(this).closest('.section').find('.more').outerHeight(h);
        jQuery(this).closest('.section').toggleClass('active');
    })

    jQuery(window).resize(function() {
        faqTabsRender();
    });
});
