<?php
$widget_name = "text_editor";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-texteditor so-wrapper so-panel widget" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <?php echo $instance['text']; ?>
</div>
