jQuery(document).ready(function() {
    if (jQuery('.widget-subscribe').length == 1) {
        jQuery('.widget-subscribe form').validate({
            //onkeyup: false,
            errorClass: "wpcf7-not-valid-tip",
            errorElement: "span",
            rules: {
                Cellphone: {
					pattern: /^[0-9-+s()]*$/
				},
			},
            messages: {
                Cellphone: {
                    required: "This field is required.",
                    pattern: "Only number and +"
                }
            },
            highlight: function(element, errorClass, validClass) {
                if(jQuery(element).attr('type') == 'checkbox'){
                    jQuery(element).closest('.wpcf7-checkbox').find('.tip-extra').addClass('add');
                }
                setTimeout(function(){
                    jQuery('.wpcf7-list-item.hide-error .wpcf7-not-valid-tip:not(.tip-extra)').remove();
                }, 100);
            },
            unhighlight: function(element, errorClass, validClass) {
                if(jQuery(element).attr('type') == 'checkbox'){
                    jQuery(element).closest('.wpcf7-checkbox').find('.tip-extra').removeClass('add');
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    }
})
