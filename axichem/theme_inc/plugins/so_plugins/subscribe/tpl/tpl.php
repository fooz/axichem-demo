<?php
$widget_name = "subscribe";
$widget_id = 'panel-' . get_the_ID().'-' . $instance['panels_info']['grid'] . '-' . $instance['panels_info']['cell'] . '-' . $instance['panels_info']['cell_index'];
?>

<div id="<?php echo $widget_id; ?>" class="widget widget-subscribe so-wrapper so-panel widget" data-animationcss="anim_opacity" data-index="<?php echo $instance['panels_info']['widget_index'] ?>" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="header text38"><?php echo $instance['header'];?></div>
            </div>
            <div class="col-12 col-md-7">
                <?php //echo $instance['text'];?>

                <form action="https://publish.ne.cision.com/Subscription/Subscribe" method="post" novalidate>
                    <input name="subscriptionUniqueIdentifier" type="hidden" value="704c76c23b" />
                    <input name="redirectUrlSubscriptionSuccess" type="hidden" value="<?php bloginfo('template_url'); ?>/newsletter?t=success" />
                    <input name="redirectUrlSubscriptionFailed" type="hidden" value="<?php bloginfo('template_url'); ?>/newsletter?t=fail" />
                    <input name="Replylanguage" type="hidden" value="<?php echo ICL_LANGUAGE_CODE; ?>" />

                    <div class="col-half">
                        <div class="label">
                            <div class="text text18">Subscription Feed*</div>
                            <p>
                                <span class="wpcf7-form-control-wrap feeds">
                                    <span class="wpcf7-form-control wpcf7-checkbox ">
                                        <label class="wpcf7-not-valid-tip tip-extra" style="display: noneX;" ><?php _e('This field is required.','axichem'); ?></label>
                                        <label class="wpcf7-list-item hide-error">
                                            <input name="informationtype" type="checkbox" value="kmk,rpt" required />
                                            <span class="wpcf7-list-item-label">Interim reports</span>
                                        </label>
                                        <label class="wpcf7-list-item hide-error">
                                            <input name="informationtype" type="checkbox" value="rdv" required />
                                            <span class="wpcf7-list-item-label">Annual reports</span>
                                        </label>
                                        <label class="wpcf7-list-item hide-error last">
                                            <input name="informationtype" type="checkbox" value="prm" required />
                                            <span class="wpcf7-list-item-label">Press releases</span>
                                        </label>
                                    </span>
                                </span>
                            </p>
                        </div>
                        <div class="label">
                            <div class="text text18">Language*</div>
                            <p>
                                <span class="wpcf7-form-control-wrap language">
                                    <span class="wpcf7-form-control wpcf7-checkbox">
                                        <label class="wpcf7-not-valid-tip tip-extra" style="display: noneX;" ><?php _e('This field is required.','axichem'); ?></label>
                                        <label class="wpcf7-list-item hide-error first">
                                            <input name="Language" type="checkbox" value="en" required />
                                            <span class="wpcf7-list-item-label">English</span>
                                        </label>
                                        <label class="wpcf7-list-item hide-error last">
                                            <input name="Language" type="checkbox" value="sv" required />
                                            <span class="wpcf7-list-item-label">Swedish</span>
                                        </label>
                                    </span>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="col-half">
                        <div class="label">
                            <div class="text text18">Name Surname*</div>
                            <p>
                                <span class="wpcf7-form-control-wrap surname">
                                    <input type="text" name="Name" value="" size="40" class="wpcf7-form-control wpcf7-text width-full wpcf7-not-valid" required />
                                </span>
                            </p>
                        </div>
                        <div class="label">
                            <div class="text text18">E-mail*</div>
                            <p>
                                <span class="wpcf7-form-control-wrap email">
                                    <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text width-full wpcf7-not-valid" required />
                                </span>
                            </p>
                        </div>
                        <div class="label">
                            <div class="text text18">Company Name</div>
                            <p>
                                <span class="wpcf7-form-control-wrap company">
                                    <input type="text" name="CompanyName" value="" size="40" class="wpcf7-form-control wpcf7-text width-medium" >
                                </span>
                            </p>
                        </div>
                        <div class="label">
                            <div class="text text18">SMS (+46709123456)</div>
                            <p>
                                <span class="wpcf7-form-control-wrap phone">
                                    <input type="text" name="Cellphone" value="" size="40" class="wpcf7-form-control wpcf7-text width-short" required>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="col-full">
                        <div class="label">
                            <span class="wpcf7-form-control-wrap iagree">
                                <span class="wpcf7-form-control wpcf7-checkbox text text18 change-link normal">
                                    <label class="wpcf7-not-valid-tip tip-extra" style="display: noneX;" ><?php _e('This field is required.','axichem'); ?></label>
                                    <label class="wpcf7-list-item hide-error first">
                                        <input name="IAgree" type="checkbox" value="I agree with the Axichem Privacy Policy" required />
                                        <span class="wpcf7-list-item-label">I agree with the Axichem Privacy Policy</span>
                                    </label>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-full">
                        <input type="submit" value="Subscribe" class="" /><span class="ajax-loader"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
