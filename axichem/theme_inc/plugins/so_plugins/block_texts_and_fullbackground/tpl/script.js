var clickHandler = ('touchend' in document.documentElement ? "touchend" : "click");

jQuery(document).ready(function() {
    function blocktextsbackgroundSize() {
        jQuery('.widget-blocktextsbackground[data-center="1"]').each(function() {
            jQuery(this).find('.col-left .text').removeClass('centerH')
            jQuery(this).find('.col-right .text').removeClass('centerH')

            leftH = jQuery(this).find('.col-left .text').height()
            rightH = jQuery(this).find('.col-right .text').height()

            if(leftH != null && rightH != null){
                maxH = 0;
                if(leftH > maxH)
                    maxH = leftH;
                if(rightH > maxH)
                    maxH = rightH;

                if(leftH < rightH)
                    jQuery(this).find('.col-left .text').addClass('centerH')
                else
                    jQuery(this).find('.col-right .text').addClass('centerH')

                jQuery(this).find('.inside_relative').height(maxH)
            }
        })
    }
    blocktextsbackgroundSize();

    jQuery(window).resize(function() {
        blocktextsbackgroundSize()
    })
});
