	<footer id="footer">
		<div class="our_brands">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="header text38"><?php echo __('Our Brands', 'axichem'); ?></div>
					</div>
				</div>
			</div>
			<?php
			if (is_active_sidebar('ourbrands-sidebar'))
				dynamic_sidebar('ourbrands-sidebar');
			?>
		</div>
		<div class="footer">
			<div class="container">
				<a class="backtotop">
					<span>back to top</span>
					<svg x="0px" y="0px" width="18.694px" height="15.225px" viewBox="0 0 18.694 15.225" enable-background="new 0 0 18.694 15.225" xml:space="preserve">
						<polyline fill="none" stroke="#C3BFFF" stroke-miterlimit="10" points="10.669,14.23 17.606,7.013 10.869,0 	"/>
						<line fill="none" stroke="#C3BFFF" stroke-miterlimit="10" x1="17.799" y1="7.024" x2="0" y2="7.024"/>
					</svg>
				</a>
				<div class="row">
					<div class="col-12 col-md-3 texts text15">
						<?php
						if (is_active_sidebar('footer-sidebar'))
							dynamic_sidebar('footer-sidebar');
						?>
					</div>
					<div class="col-12 col-md-6">
						<?php
						$actId = get_the_ID();

						$menu = array();
						$menuOrg = wp_get_nav_menu_items('Menu footer');
						?>
						<?php
						// Build structure menu
						// ========================================================
						foreach ($menuOrg as $item) {
							$href = $item->url;

							if ($item->menu_item_parent == 0) {
								$menu[$item->ID] = $item->title;
							}
							if ($item->menu_item_parent != 0) {
								if (!is_array($menu[$item->menu_item_parent])) {
									$menu[$item->menu_item_parent] = array();
								}
								$menu[$item->menu_item_parent][$item->ID] = '';
							}
						}

						// Display menu
						// ========================================================
						?>
						<div class="links" id="menu-footer">
							<div class="cl cl-half">
							<?php
							$cc = 1;
							$clGenerate = false;
							foreach ($menu as $key => $item) {
								echo '<div class="text15" data-id="'. $key .'">';
								echo findId($key, $menuOrg, '', $actId);
								echo '</div>';
								if($cc >= ceil(sizeof($menu)/2) && $clGenerate == false){
	                                echo '</div><div class="cl cl-half">';
	                                $clGenerate = true;
	                            }
								$cc++;
							}
							?>
							</div>
							<div class="clear"></div>
						</div>
					</div>

					<div class="col-12 col-md-3">
						<?php
						$menu = array();
						$menuOrg = wp_get_nav_menu_items('Menu Additional');
						?>
						<?php
						// Build structure menu
						// ========================================================
						foreach ($menuOrg as $item) {
							$href = $item->url;

							if ($item->menu_item_parent == 0) {
								$menu[$item->ID] = $item->title;
							}
							if ($item->menu_item_parent != 0) {
								if (!is_array($menu[$item->menu_item_parent])) {
									$menu[$item->menu_item_parent] = array();
								}
								$menu[$item->menu_item_parent][$item->ID] = '';
							}
						}

						// Display menu
						// ========================================================
						?>
						<div class="links" id="menu-footer">
							<div class="cl cl-half">
							<?php
							foreach ($menu as $key => $item) {
								echo '<div class="text15" data-id="'. $key .'">';
								echo findId($key, $menuOrg, '', $actId);
								echo '</div>';
							}
							?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="socials">
		                    <?php
		                    if (is_active_sidebar('social-sidebar'))
		                        dynamic_sidebar('social-sidebar');
		                    ?>
		                </div>
					</div>
					<div class="col-12">
						<hr/>
					</div>
					<div class="col-6">
						<div class="texts text13">© aXichem AB 2018</div>
					</div>
					<div class="col-6">
						<div class="texts text13 text-align-right">design by <a href="http://www.balsamstudio.com/" target="_blank">balsamstudio.</a></div>
					</div>
				</div>
			</div>
		</div>



		<div class=""></div>

	</footer>
	<?php
		wp_footer();
	?>
</body>
</html>
