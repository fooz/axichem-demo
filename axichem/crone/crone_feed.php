<?php
require('wp-load.php');
//require('../../../../wp-load.php');

// CONFIG
// ===========================================================================================

// FEED
// All Releases  SV
$urlFeedSv = 'http://publish.ne.cision.com/papi/NewsFeed/8E6F6AE0CB1D4423A8FF15FB0652B3A9?format=json&detailLevel=detail&PageSize=100&pageIndex=1';

// All Releases EN
$urlFeedEn = 'http://publish.ne.cision.com/papi/NewsFeed/C9250F0825304F1EBE77F6BCD9D01845?format=json&detailLevel=detail&PageSize=100&pageIndex=1';


// START
// ===========================================================================================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




// EN
// ====================================================


$fileFeedEn = file_get_contents($urlFeedEn);
$jsonFeedEn = json_decode($fileFeedEn);
foreach ($jsonFeedEn->Releases as $key => $value) {
	echo '<h1>'.$key.'</h1>';
	echo $value->Id.'<br />';
	echo $value->EncryptedId.'<br />';
	echo $value->PublishDate.'<br />';
	echo $value->InformationType.'<br />';
	echo 'lang: '.$value->LanguageCode.'<br />';
	echo $value->CisionWireUrl.'<br /><br />';
	echo $value->Title.'<br />';

	$bodyMod = $value->HtmlBody;
	$bodyMod = preg_replace('/style[^>]*/i', "$2", $bodyMod);

		// Add post
		// =======================================================

		$args = array(
			'post_status' => 'publish',
			'post_type' => 'news',
			'meta_key' => 'Id',
			'meta_value' => $value->Id
		);
		$checkExist = get_posts($args);
		if(sizeof($checkExist) == 0){

			$post_content = ' ';
			if(strlen($value->HtmlBody) > 0)
					$post_content = preg_replace('/style[^>]*/i', "$2", $value->HtmlBody);

			$createPost = array(
					'post_title' => $value->Title,
					'post_date' => $value->PublishDate,
					'post_content' => $post_content,
					'post_status' => 'publish',
					'post_type' => 'news'
			);
			$idNew = wp_insert_post($createPost);
			update_post_meta($idNew, 'Id', $value->Id);
			update_post_meta($idNew, 'EncryptedId', $value->EncryptedId);
			update_post_meta($idNew, 'LanguageCode', $value->LanguageCode);
			update_post_meta($idNew, 'CisionWireUrl', $value->CisionWireUrl);
			update_post_meta($idNew, 'FilesUrl', $value->Files[0]->Url);

			$slug_cat = $value->InformationType;
			wp_set_object_terms($idNew, strtolower($slug_cat), 'type' );

			if($value->InformationType == 'PRM'){
				if($value->IsRegulatory == 1){
					$cat_ids = array('prm', 'prm_regulatory');
					wp_set_object_terms($idNew, $cat_ids, 'type' );
				}
			}

		}else{
				echo '- exist post';
		}
}


// SV
// ====================================================

$fileFeedSv = file_get_contents($urlFeedSv);
$jsonFeedSv = json_decode($fileFeedSv);
foreach ($jsonFeedSv->Releases as $key => $value) {
	echo '<h1>'.$key.'</h1>';
	echo $value->Id.'<br />';
	echo $value->EncryptedId.'<br />';
	echo $value->PublishDate.'<br />';
	echo $value->InformationType.'<br />';
	echo 'lang: '.$value->LanguageCode.'<br />';
	echo $value->CisionWireUrl.'<br /><br />';
	echo $value->Title.'<br />';
	echo $value->Files[0]->Url.'<br />';

	$bodyMod = $value->HtmlBody;
	$bodyMod = preg_replace('/style[^>]*/i', "$2", $bodyMod);

    // Add post
    // =======================================================

    $args = array(
      'post_status' => 'publish',
      'post_type' => 'news',
      'meta_key' => 'Id',
    	'meta_value' => $value->Id
    );
    $checkExist = get_posts($args);
    if(sizeof($checkExist) == 0){

      $post_content = ' ';
      if(strlen($value->HtmlBody) > 0)
          $post_content = preg_replace('/style[^>]*/i', "$2", $value->HtmlBody);

      $createPost = array(
          'post_title' => $value->Title,
          'post_date' => $value->PublishDate,
          'post_content' => $post_content,
          'post_status' => 'publish',
          'post_type' => 'news'
      );
      $idNew = wp_insert_post($createPost);
      update_post_meta($idNew, 'Id', $value->Id);
      update_post_meta($idNew, 'EncryptedId', $value->EncryptedId);
      update_post_meta($idNew, 'LanguageCode', $value->LanguageCode);
      update_post_meta($idNew, 'CisionWireUrl', $value->CisionWireUrl);
      update_post_meta($idNew, 'FilesUrl', $value->Files[0]->Url);

			$slug_cat = $value->InformationType;
			wp_set_object_terms($idNew, strtolower($slug_cat), 'type' );

			if($value->InformationType == 'PRM'){
				if($value->IsRegulatory == 1){
					$cat_ids = array('prm', 'prm_regulatory');
					wp_set_object_terms($idNew, $cat_ids, 'type' );
				}
			}


    }else{
        echo '- exist post';
    }
}



// ===========================================================================================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// END


// $message = ob_get_contents();
// ob_end_clean();
//
// $fileLocation = "report/" . date('Y-m-d H:i:s') . " Feed.txt";
// $file = fopen($fileLocation, "w");
// fwrite($file, $message);
// fclose($file);
?>
