<?php
/*
Przygotowanie na bazie frameworka dla Wordpress fpweb.pl
*/
/*
function change_dashboard_primary_title () {
  return "Nowości na stronie";
}
function change_dashboard_primary_feed() {
  return "";
}

add_filter("dashboard_primary_feed","change_dashboard_primary_feed");
add_filter("dashboard_primary_title","change_dashboard_primary_title");
*/


// REMOVE META BOXES FROM WORDPRESS DASHBOARD
//Completely remove various dashboard widgets (remember they can also be HIDDEN from admin)
/*
	remove_meta_box( 'dashboard_quick_press',   'dashboard', 'side' );      //Quick Press widget
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );      //Recent Drafts
	remove_meta_box( 'dashboard_primary',       'dashboard', 'side' );      //WordPress.com Blog
	remove_meta_box( 'dashboard_secondary',     'dashboard', 'side' );      //Other WordPress News
	remove_meta_box( 'dashboard_incoming_links','dashboard', 'normal' );    //Incoming Links
	remove_meta_box( 'dashboard_plugins',       'dashboard', 'normal' );    //Plugins
 */
function example_remove_dashboard_widgets() {
	if(!current_user_can('administrator')) {
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'side' );
	//remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	//remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	//remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	//remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	} else {
	//remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	//remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	}
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' );

?>
