<?php  
/* 
Plugin Name: Optymalizacja Systemu
Plugin URI:
Description: Optymalizuje wyświetlanie kilku istotnych elementów pod kątem bezpieczeństwa
Version: 2
Date: 25.11.2013
Author: Wojciech Zdziejowski, Grupa FingerprintWeb.pl
Author URI:
License: GPL2 

źródło: http://net.tutsplus.com/tutorials/wordpress/your-first-wordpress-plugin-simple-optimization/ 
*/  



// Clean up wp_head  

// Remove that junk from my wp_head
 remove_action('wp_head', 'rsd_link'); // Removes the Really Simple Discovery link
 remove_action('wp_head', 'wlwmanifest_link'); // Removes the Windows Live Writer link
 remove_action('wp_head', 'feed_links', 2); // Removes the RSS feeds remember to add post feed maunally (if required) to header.php
 remove_action('wp_head', 'feed_links_extra', 3); // Removes all other RSS links
 remove_action('wp_head', 'index_rel_link'); // Removes the index page link
 remove_action('wp_head', 'start_post_rel_link', 10, 0); // Removes the random post link
 remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Removes the parent post link
 remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Removes the next and previous post links
 



// Remove curly quotes  
remove_filter('the_content', 'wptexturize');  
remove_filter('comment_text', 'wptexturize');  
  
// Allow HTML in user profiles  
remove_filter('pre_user_description', 'wp_filter_kses');  

// Remove version
remove_action('wp_head', 'wp_generator'); // Removes the WordPress version
function wpt_remove_version() {  
   return '';  
}  
add_filter('the_generator', 'wpt_remove_version');  

//============================================================
//Optimize Database  
function optimize_database(){  
    global $wpdb;  
    $all_tables = $wpdb->get_results('SHOW TABLES',ARRAY_A);  
    foreach ($all_tables as $tables){  
        $table = array_values($tables);  
        $wpdb->query("OPTIMIZE TABLE ".$table[0]);  
    }  
}  
function simple_optimization_cron_on(){  
    wp_schedule_event(time(), 'daily', 'optimize_database');  
}
function simple_optimization_cron_off(){  
    wp_clear_scheduled_hook('optimize_database');  
}  
register_activation_hook(__FILE__,'simple_optimization_cron_on');  
register_deactivation_hook(__FILE__,'simple_optimization_cron_off');



?>