<?php

// Lista wtyczek
require_once(PLUGIN_DIRECTORY.'dashboard.php');
require_once(PLUGIN_DIRECTORY.'optymalizacja-fpweb.php');
require_once(PLUGIN_DIRECTORY.'customizacja-fpweb.php');

/**
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.5.2
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once (PLUGIN_DIRECTORY.'TGMPA-TGM-Plugin-Activation-2.5.5-0/class-tgm-plugin-activation.php');
require_once (PLUGIN_DIRECTORY.'plugin-activation-fpweb.php');
require_once (PLUGIN_DIRECTORY.'scalable-vector-graphics.php');
?>
